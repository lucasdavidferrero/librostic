import {LoginError} from '../../common/models/login-error.model';

export function SigninErrors(code: string): LoginError {
  const title = 'Ocurrió un error mientras intentabamos autenticarte.';
  let msg = '';
  switch (code) {

    case 'auth/cancelled-popup-request':
      msg = 'Solo se puede abrir una ventana a la vez.';
      return new LoginError(title, msg );

    case 'auth/popup-blocked':
      msg = `Al parecer tu navegador bloqueó la ventana de autenticación.
            Prueba configurar tu navegador o utilizar otro.`;
      return new LoginError(title, msg);

    case 'auth/popup-closed-by-user':
      msg = `Al parecer cerraste la ventana de autenticación antes de verificar tu cuenta.`;
      return new LoginError(title, msg);

    case 'auth/web-storage-unsupported':
      msg = `Al parecer tu navegador no soporta web storage o ésta característica esta desactivada.`;
      return new LoginError(title, msg);
    case 'auth/account-exists-with-different-credential':
      msg = `Al parecer un mismo correo esta siendo usado con diferentes métodos de autenticación. Vuelve a utilizar el servicio
      que usaste la primera vez. Recuerda que utilizamos tu email como usuario.`;
      return new LoginError(title, msg);
    default:
      msg = `Se produjo un error inesperado, intentelo de nuevo. Si sigue sin poder ingresar 
            contactese con nosotros`;
      // Later we should post the error in a firebase node to see in more detail the error.
      return new LoginError(title, msg);
  }
}
