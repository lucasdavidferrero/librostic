import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {AuthService} from '../../common/services/auth.service';
import * as firebase from 'firebase/app';
import { SigninErrors } from './login.error';
import {LoginError} from '../../common/models/login-error.model';
import {ActivatedRoute, Router} from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errorAlert: LoginError;
  showError = false;
  showBannedError = false;
  bannedReason: string;
  bannedEmail: string;
  constructor(private authService: AuthService,
              private changeRef: ChangeDetectorRef,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.queryParams
      .subscribe(
        (query) => {
          // SHOULD BE A STRING. REMOVE THE + LATER
          // TODO Remover el + ya que el ID va a ser generado automaticamente por firebase.
          const bannedId = query.bannedId;
          // Go to the database and get the banned reason.
          firebase.database().ref(`Banned/${bannedId}`)
            .once('value')
            .then(
              (snapshot: firebase.database.DataSnapshot) => {
                if (snapshot.exists()) {
                  this.bannedReason = snapshot.val().reason;
                  this.bannedEmail = snapshot.val().email;
                  this.showError = false;
                  this.showBannedError = true;
                  this.changeRef.detectChanges();
                }
              }
            );
        }
      );
  }
  onFacebookLogin() {
    this.goToRoot();
    this.authService.signinWithFacebook()
      .catch(
        (err: firebase.FirebaseError) => {
          if (err) {
            this.errorAlert = SigninErrors(err.code);
            this.showError = true;
          }
          this.changeRef.detectChanges();
        }
      );
  }
  onGoogleLogin() {
    this.goToRoot();
    this.authService.signinWithGoogle()
      .catch(
        (err: firebase.FirebaseError) => {
          if (err) {
            this.errorAlert = SigninErrors(err.code);
            this.showError = true;
          }
          this.changeRef.detectChanges();
        }
      );
  }
  onGithubLogin() {
    this.goToRoot();
    this.authService.signinWithGithub()
      .catch(
        (err: firebase.FirebaseError) => {
          if (err) {
            this.errorAlert = SigninErrors(err.code);
            this.showError = true;
          }
          this.changeRef.detectChanges();
        }
      );
  }

  onTwiterLogin() {
    this.goToRoot();
    this.authService.signinWithTwitter()
      .catch(
        (err: firebase.FirebaseError) => {
          if (err) {
            console.error(err);
            this.errorAlert = SigninErrors(err.code);
            this.showError = true;
          }
          this.changeRef.detectChanges();
        }
      );

  }

  goToRoot() {
    this.router.navigate(['/login']);
  }
  // close the error
  onDismissError() {
    this.showError = false;
    this.showBannedError = false;
    this.changeRef.detectChanges();
  }
}
