import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth/auth.module';
import { DisqusModule } from 'ngx-disqus';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
// import { ShareButtonsModule } from 'ngx-sharebuttons';

// angular fire modules
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';

import * as firebase from 'firebase';


import { AppComponent } from './app.component';
import { HomeComponent } from './shared/home/home.component';
import { MenuComponent } from './shared/menu/menu.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LoginComponent } from './public/login/login.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { BookItemComponent } from './shared/home/book-item/book-item.component';
import { BookDetailComponent } from './shared/book-detail/book-detail.component';
import { CommentsComponent } from './shared/book-detail/comments/comments.component';
import { SearchResultComponent } from './shared/search-result/search-result.component';
import { ItemResultComponent } from './shared/search-result/item-result/item-result.component';
import { CategoryListComponent } from './shared/categories/category-list/category-list.component';
import { SearchNotFoundComponent } from './shared/search-result/search-not-found/search-not-found.component';
import { CategoryDetailComponent } from './shared/categories/category-detail/category-detail.component';
import { AuthorComponent } from './shared/author/author.component';
import { AdvertisementComponent } from './shared/advertisement/advertisement.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';



import { AuthService } from './common/services/auth.service';
import { UserService } from './common/services/user.service';
import { UserConnectedService } from './common/services/user-connected.service';
import { AuthorService } from './common/services/author.service';
import { BookService } from './common/services/book.service';
import { CategoryService } from './common/services/category.service';
import { DashboardUsersService } from './common/services/dashboard/dashboard-users.service';
import { BanUserService } from './common/services/dashboard/ban-user.service';
import { WelcomeService } from './common/services/dashboard/welcome.service';
import { BookReportService } from './common/services/book-report.service';
import { ErrorsService } from './common/services/errors.service';
import { LikeBookService } from './common/services/like-book.service';
import { TitleService } from './common/services/title.service';
import { SearchService } from './common/services/search.service';
import { AdvertisementService } from './common/services/advertisement.service';
import { AuthGuardService } from './auth/auth-guard.service';

import { UserModel } from './common/models/user.model';
import { FormBookService } from './common/services/form-book.service';

import { FileManager } from './auth/file-manager';
import { AdminGuardService } from './+admin/admin-guard.service';
import { UnloggedGuardService } from './auth/unlogged-guard.service';
import {ShareButtonsModule} from 'ngx-sharebuttons';





const firebaseConfig = {
  apiKey: 'AIzaSyB8j6381d88ZUSSQPWnx0AD-zg5rNTgPg0',
  authDomain: 'librostic-fa290.firebaseapp.com',
  databaseURL: 'https://librostic-fa290.firebaseio.com',
  projectId: 'librostic-fa290',
  storageBucket: 'librostic-fa290.appspot.com',
  messagingSenderId: '180605460529'
};

firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    FooterComponent,
    LoginComponent,
    SpinnerComponent,
    BookItemComponent,
    BookDetailComponent,
    CommentsComponent,
    SearchResultComponent,
    ItemResultComponent,
    SearchNotFoundComponent,
    CategoryListComponent,
    CategoryDetailComponent,
    AuthorComponent,
    AdvertisementComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
    AuthModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    DisqusModule.forRoot('librostic'),
    ShareButtonsModule.forRoot()
  ],
  providers: [
    AuthService,
    AngularFireAuth,
    UserService,
    UserConnectedService,
    AuthorService,
    BookService,
    CategoryService,
    FormBookService,
    FileManager,
    DashboardUsersService,
    BanUserService,
    WelcomeService,
    BookReportService,
    ErrorsService,
    LikeBookService,
    TitleService,
    SearchService,
    AdvertisementService,
    AuthGuardService,
    AdminGuardService,
    UnloggedGuardService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private afAuth: AngularFireAuth,
              private authService: AuthService,
              private userService: UserService) {
    // checking if the user is login or not, if it is we show different UI elements like the profile
    // dropdown on the main menu.
    this.afAuth.authState
      .subscribe(
        (data: firebase.User) => {
          if (data) {
            this.authService.refreshTokenAndGetIt();
            const buildedUser = new UserModel(data.uid, data.displayName, data.email,
                data.photoURL, data.providerId);
            this.userService.user = buildedUser;

            this.authService.isAdmin(this.userService.user.uid);
          } else {
            console.error('user is signout', data);
          }
        },
        () => {
        },
        () => {
        }
      );
  }

}
