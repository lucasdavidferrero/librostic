import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.component.html',
  styleUrls: ['./advertisement.component.css']
})
export class AdvertisementComponent implements OnInit {
  @Input('adKind') adType: string;
  constructor() { }

  ngOnInit() {
    // We got 2 kind of ads. Large leaderboard and panorama.
  }

}
