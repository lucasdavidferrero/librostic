import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserService } from '../../common/services/user.service';
import { AuthService } from '../../common/services/auth.service';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

declare var $: any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, AfterViewInit {
  isAdmin = false;
  isAuth: Observable<firebase.User>;

  appendMenu = false;
  constructor(private afAuth: AngularFireAuth,
              public userService: UserService,
              public authService: AuthService,
              private router: Router,) { }

  ngOnInit() {
    this.isAuth = this.afAuth.authState;
    this.userService.isUserAdmin
      .subscribe(
        (bit: boolean) => {
          this.isAdmin = bit;
        }
      );
  }

  ngAfterViewInit() {
    $('.ui.dropdown')
      .dropdown()
    ;
  }

  onSignout() {
    this.authService.signout();
  }
  // These functions are just for UI propose.
  onDropdownEnter() {
    $('.ui.dropdown.item').addClass('active visible');

    // show elements of the dropdown.
    $('.menu.custom-menu').transition('slide down');
  }

  onDropdownLeave() {
    // hide menu and dropdown
    $('.ui.dropdown.item').removeClass('active visible');
    $('.menu.custom-menu').transition('slide down');
  }

  onShowpopup() {
    $('.item.showPopup')
      .popup('show');
  }
  onHidepopup() {
    $('.item.showPopup')
      .popup('hide');
  }

  onClickSearch(inputQuery: HTMLInputElement, inputSelect: HTMLSelectElement) {
    const query = inputQuery.value.trim().replace(/\s+/g, '-').toLowerCase()
    const selectedOption = inputSelect.value;

    if (query.length > 1) {
      this.router.navigate([`/search/${selectedOption}/${query}`]);
    }
  }

  onAppendMenu() {
    this.appendMenu = !this.appendMenu;
  }
}
