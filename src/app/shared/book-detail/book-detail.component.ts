import { AfterContentInit, Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { BookService } from '../../common/services/book.service';
import { BookModel } from '../../common/models/book.model';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { UserService } from '../../common/services/user.service';
import { UserModel } from '../../common/models/user.model';
import { NgForm } from '@angular/forms';
import { BookReportService } from '../../common/services/book-report.service';
import { ErrorsService } from '../../common/services/errors.service';
import { LikeBookService } from '../../common/services/like-book.service';
import * as firebase from 'firebase';
import {TitleService} from '../../common/services/title.service';
import {AdvertisementService} from '../../common/services/advertisement.service';

declare var $: any;
declare var window: any;

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit, AfterContentInit {
  book: BookModel;
  downloadLink;
  showMessage = false;
  userInfo: UserModel;
  showIframe = false;

  currentURL: string;

  errorReport: string;
  successReport: string;
  alreadyLiked: boolean;
  adEnabled = false;
  constructor(private activatedRoute: ActivatedRoute,
              private bookService: BookService,
              private sanitizer: DomSanitizer,
              public userService: UserService,
              private bfService: BookReportService,
              private errorService: ErrorsService,
              private likeBookService: LikeBookService,
              private titleService: TitleService,
              private router: Router,
              private adService: AdvertisementService) { }

  ngOnInit() {
    this.callAd();
    window.scrollTo(0, 0);
    this.currentURL = window.location.href;
    // The key that's been passed from the URL.
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          const passedKey = params['key'];
          // Getting the book from Firebase.
          this.bookService.getBookByKey(passedKey)
            .then(
              (snap: firebase.database.DataSnapshot) => {
                if (snap.exists()) {
                  // There are data. Show to book.
                  this.book = this.bookService.fillBookObject(snap);
                  this.listenLikes(this.book.$key);
                  this.listenBookLikes(this.book.$key);
                  this.titleService.setTitle(this.book.title);
                  // Get user information.
                  this.getUserInfo(this.book.uploadedUser);
                  // Build the download link
                  this.buildDownloadLink(this.book.downloadOption, this.book.downloadURL);

                  // Ask is the user is log in. If it is, check if already like the book that was found.
                  if (this.userService.user) {
                    this.likeBookService.checkIfUserAlreadyLikeBook(this.book.$key, this.userService.user.uid)
                      .then(
                        (snapshot: firebase.database.DataSnapshot) => {
                          this.alreadyLiked = snapshot.exists();
                        }
                      );
                  }
                } else {
                  // No data was found with the given key. Redirect to 404.
                  this.router.navigate(['**']);
                }
              }
            )
            .catch(
              (err: Error) => {
                // Send the error to error's node and redirect to 404.
                this.errorService.fetchingBookError(err);
              }
            );
        }
      );
  }
  ngAfterContentInit() {
    $('.ui.accordion')
      .accordion()
    ;
  }

  private buildDownloadLink(downloadOption: string, downloadURL: string) {
    if (downloadOption === 'googleDrive') {
      // At this time the building link for direct download of google's drive files is:
      // https://drive.google.com/uc?export=download&id=FILE_ID
      this.downloadLink = `https://drive.google.com/uc?export=download&id=${downloadURL}`;
    } else {
      this.downloadLink = downloadURL;
    }
  }

  cleanURL(oldURL ): SafeUrl {
    return   this.sanitizer.bypassSecurityTrustResourceUrl(oldURL);
  }

  onLike() {
    if (!this.userService.user) {
      // Don't update the likes of the book.
      this.showMessage = true;
    } else {
      // TODO finish the following:
      // The user is logged in, update the likes of the current book.
      this.likeBookService.saveLike(this.book.$key, this.userService.user.uid);
    }
  }

  private getUserInfo(key: string) {
    this.userService.getUserByKey(key)
      .then(
        (snap: firebase.database.DataSnapshot) => {
          if (snap.exists()) {
            const data = snap.val();
            this.userInfo = new UserModel(Object.keys(data)[0], data.displayName, data.email, data.photoURL, data.providerId);
          }
        }
      ).catch(
      (err: Error) => {
        console.error(err);
      }
    );
  }

  onSubmitReport(f: NgForm) {
    if (f.valid) {
      const promiseResult = this.bfService.checkReport(this.book.$key, f.controls.reportOption.value,
        this.userService.user.uid, this.userService.user.displayName);

      if (promiseResult !== null) {
        promiseResult
          .then(
            (snapshot: firebase.database.DataSnapshot) => {
              if (!snapshot.exists()) {
                // If data doesn't exists means that the user never report the book with the selected option (Everything OK).
                this.bfService.pushReport(snapshot.ref)
                  .then(
                    () => {
                      this.successReport = 'Reporte enviado satisfactoriamente.';

                       // Increment counters
                      this.bfService.incrementReportCounter(snapshot.ref);
                    }
                  )
                  .catch(
                    (err: Error) => {
                      // Save the error in the DB.
                      this.errorReport = 'Ocurrió un error inesperado. No te preocupes, hemos guardado el error.';
                      this.errorService.saveErrorWhenReportBook(err);
                    }
                  );
              } else {
                // At this point the user has already send  the report for the book with the same selected option. (Denied report.)
                this.errorReport = 'Ya has reportado este libro anteriormente.';
              }
            }
          );
      } else {
        // The reported option is wrong. The user may change it in the UI and it's trying to send
        // dummy information.
        this.errorReport = 'Selecciona un motivo válido.';
      }
    }
  }



  private listenLikes(bookID: string) {
    const bookRef = firebase.database().ref(`books/${bookID}/likes`);

    bookRef.on('value', (snap: firebase.database.DataSnapshot) => {
      this.book.likes = snap.val();
    });
  }

  private listenBookLikes(bookID: string) {
    const blRef = firebase.database().ref(`bookUserLike/${this.userService.user.uid}/books/${bookID}`);
    blRef.on('value', (snap: firebase.database.DataSnapshot) => {
      // console.log(snap.hasChild(this.book.$key));
      this.alreadyLiked = snap.val();
    });
  }
  onClickReport() {
    $('.ui.modal')
      .modal('show')
    ;
  }

  onCloseReportModal() {
    $('.ui.modal')
      .modal('hide')
    ;
  }

  onCloseMessage() {
    this.showMessage = false;
    this.errorReport = '';
    this.successReport = '';
  }

  private callAd() {
    this.adService.isAdEnabled()
      .then(
        (snap: firebase.database.DataSnapshot) => {
          this.adEnabled = snap.val();
        }
      );
  }
}
