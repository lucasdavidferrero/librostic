import { Component, OnInit } from '@angular/core';
import { BookService } from '../../common/services/book.service';
import { BookModel } from '../../common/models/book.model';
import { AuthorMetaModel } from '../../common/models/author-meta.model';
import { CategoryMetaModel } from '../../common/models/category-meta.model';
import { TitleService } from '../../common/services/title.service';
import * as firebase from 'firebase';
import {AdvertisementService} from '../../common/services/advertisement.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  shallowBooks = [];
  lastKey: string;
  TOTAL_BOOKS: number;
  private LIMIT_TO_LAST = 20; // The same value that is used in getLazyBooks()

  notMoreBooks = false;
  visibleBooks = [];
  isLoading = false;

  enabledAd = false;
  constructor(private bookService: BookService,
              private titleService: TitleService,
              private adService: AdvertisementService) { }

  ngOnInit() {
    this.adCall();
    window.scrollTo(0, 0);
    this.isLoading = true;
    this.bookService.getShallowBooks()
      .subscribe(
        (data) => {
          this.TOTAL_BOOKS = data.length;
        }
      );

    this.bookService.getLazyBooks(this.lastKey)
      .then(
        (data: firebase.database.DataSnapshot) => {
          this.shallowBooks = data.val();
          this.lastKey = Object.keys(this.shallowBooks).shift();
          this.visibleBooks = this.convertObjectIntoBook(data.val());
          this.visibleBooks.shift();
          this.visibleBooks.reverse();
          this.isLoading = false;
        }
      );

    this.titleService.setDefaultTitle();
  }
  onLoadMoreBooks() {
    if (this.TOTAL_BOOKS === this.visibleBooks.length + this.LIMIT_TO_LAST && this.TOTAL_BOOKS !== this.visibleBooks.length) {
      // Traer últimos 2(los 2 primeros). Esto dependera del LimitToLast.
      this.isLoading = true;
      this.bookService.getFirstestBooks()
        .then(
          (data: firebase.database.DataSnapshot) => {
            const newBooks = this.convertObjectIntoBook(data.val()).reverse();
            for (let i = 0; i < newBooks.length; i++) {
              this.visibleBooks.push(newBooks[i]);
            }
            this.isLoading = false;
          }
        );
    } else if (this.TOTAL_BOOKS === this.visibleBooks.length) {
      this.notMoreBooks = true;
      // TODO FIX...
      console.log('There is no more books to show');
    } else {
      this.isLoading = true;
      this.bookService.getLazyBooks(this.lastKey)
        .then(
          (data: firebase.database.DataSnapshot) => {
            this.shallowBooks = data.val();
            this.lastKey = Object.keys(this.shallowBooks).shift();
            const newBooks = this.convertObjectIntoBook(data.val()).reverse();
            for (let i = 0; i < newBooks.length; i++) {
              this.visibleBooks.push(newBooks[i]);
            }
            this.visibleBooks.pop();
            this.isLoading = false;
          }
        );
    }
  }

  private convertObjectIntoBook(data): BookModel[] {
    const books: BookModel[] = [];
    const keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      const k = keys[i];
      const authors: AuthorMetaModel[] = [];
      const authorKeys = Object.keys(data[k].authors);
      for (let j = 0; j < authorKeys.length; j++) {
        const authorK = authorKeys[j];
        authors.push(new AuthorMetaModel(authorK, data[k].authors[authorK].authorName));
      }
      books.push( new BookModel(k, authors, data[k].briefDescription,
        new CategoryMetaModel({key: data[k].category.mainCategory.keyMainCategory,
          name: data[k].category.mainCategory.name},
          {key: data[k].category.subCategory.keySubCategory,
          name: data[k].category.subCategory.name}), data[k].coverImage, data[k].downloadOption, data[k].downloadURL,
        data[k].edition, data[k].likes, data[k].numberOfPages, data[k].releaseDate,
        data[k].title, data[k].uploadedUser) );
    }
    return books;
  }

  private adCall() {
    this.adService.isAdEnabled()
      .then(
        (snap: firebase.database.DataSnapshot) => {
          this.enabledAd = snap.val();
        }
      );
  }
}
