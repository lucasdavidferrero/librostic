import { Component, Input, OnInit } from '@angular/core';
import { BookModel } from '../../../common/models/book.model';

@Component({
  selector: 'app-item-result',
  templateUrl: './item-result.component.html',
  styleUrls: ['./item-result.component.css']
})
export class ItemResultComponent implements OnInit {
  @Input() bookInput: BookModel;
  constructor() { }

  ngOnInit() {
  }

}
