import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { SearchService } from '../../common/services/search.service';
import { ErrorsService } from '../../common/services/errors.service';
import { BookModel } from '../../common/models/book.model';
import { BookService } from '../../common/services/book.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  isBookSearch = false;
  isAuthorSearch = false;
  isLoading = false;
  booksFound: BookModel[];

  searchOption: string;
  searchTerm: string;
  constructor(private activatedRoute: ActivatedRoute,
              private searchService: SearchService,
              private errorService: ErrorsService,
              private bookService: BookService,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          const query = params['query'];
          const option = params['option'];
          this.searchOption = option;
          this.searchTerm = query;
          // Based on the given option we show different components.
          // for example: If the option is 'author' we should show a different component rather than show
          // just one for all the options because the author page may look different.
          const isOptionValid = this.checkingOption(option);
          if (isOptionValid) {
            this.fetchDataBasedOnOption(query);
          }
        }
      );
  }

  private checkingOption(option: string): boolean {
    // If the option exists we return true anything else
    // return false (meaning that the option doesn't exists)
    switch (option) {
      case 'books' :
        this.isBookSearch = true;
        return true;
      case 'authors' :
        this.isAuthorSearch = true;
        return true;
      default :
      // TODO Redirect to 404.
        return false;
    }
  }

  private fetchDataBasedOnOption(query: string) {
    this.booksFound = [];
    this.isLoading = true;
    if (this.isBookSearch) {
      this.searchByBook(query);
    }
  }

  private searchByBook(query: string) {
    this.searchService.searchByBookName(query)
      .then(
        (snap: firebase.database.DataSnapshot) => {
          if (snap.exists()) {
            // Render the books...
            this.booksFound = this.bookService.convertObjectsIntoBooks(snap.val());
          } else {
            this.booksFound = [];
            this.router.navigate(['/book-not-found']);
          }
          this.isLoading = false;
        }
      ).catch(
      (err: Error) => {
        const refError = this.errorService.setReason('search-result-book');
        this.errorService.saveError(refError, err);
        this.isLoading = false;
      }
    );
  }

  private searchByAuthor(query: string) {
    // TODO Next version
  }
}
