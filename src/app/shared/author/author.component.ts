import { Component, OnInit } from '@angular/core';
import {AuthorService} from '../../common/services/author.service';
import {AuthorModel} from '../../common/models/author.model';
import {ActivatedRoute, Params} from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {
  author: AuthorModel;

  authorBooks: {bookKey: string, bookTitle?: string, bookPhoto: string}[];
  isLoading = false;
  showBooks = true;
  showBio = false;
  constructor(private authorService: AuthorService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.isLoading = true;
    this.activatedRoute.params
      .subscribe(
        (params: Params) => {
          const authorKey = params['authorID'];
          this.authorService.getAuthorByKey(authorKey)
            .then(
              (snap: firebase.database.DataSnapshot) => {
                if (snap.val()) {
                  this.author = this.authorService.fillAuthorModel(snap.val(), authorKey);
                  this.authorBooks = this.authorService.fillAuthorBooks(snap.val().books);
                  this.isLoading = false;
                } else {
                  this.isLoading = false;
                  // TODO No author was faound with the given ID. Redirect 404
                }
              }
            )
            .catch(
              (err: Error) => {
                this.isLoading = false;
                console.error(err);
              }
            );
        }
      );
  }

  changeToBooks() {
    this.showBio = false;
    this.showBooks = true;
  }

  changeToBio() {
    this.showBooks = false;
    this.showBio = true;
  }
}
