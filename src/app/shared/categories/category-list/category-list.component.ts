import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../common/services/category.service';
import { CategoryModel } from '../../../common/models/category.model';
import * as firebase from 'firebase';
import {ErrorsService} from '../../../common/services/errors.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {
  categories: CategoryModel[];

  errorMessage: string;
  isLoading = false;
  constructor(private categoryService: CategoryService,
              private errorService: ErrorsService) { }

  ngOnInit() {
    this.isLoading = true;
    this.categoryService.getAllCategories()
      .then(
        (snap: firebase.database.DataSnapshot) => {
          if (snap.val()) {
            this.categories = this.categoryService.fillAllCategories(snap);
            this.categories = this.categoryService.orderCategoryAndSubCategory(this.categories); // here we order the categories.
            this.isLoading = false;
          } else {
            this.errorMessage = 'No encontramos ninguna categoría.';
            this.isLoading = false;
          }
        }
      )
      .catch(
        (err: Error) => {
          this.errorService.saveError(this.errorService.setReason('fetchingCategories'), err);
          this.errorMessage = err.message;
          this.isLoading = false;
        }
      );
  }

}
