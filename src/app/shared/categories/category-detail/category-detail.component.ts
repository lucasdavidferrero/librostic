import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../common/services/category.service';
import {ActivatedRoute, Params} from '@angular/router';
import * as firebase from 'firebase';
import { SubCategoryBookModel } from '../../../common/models/sub-category-book.model';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {


  heroImage: string;
  subCategoryName: string;
  subCategories: SubCategoryBookModel;
  errorMessage: string;

  isLoading = false;
  constructor(private categoryService: CategoryService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.isLoading = true;
    this.activatedRoute.params
      .subscribe(
        (param: Params) => {
          const mainCategorykey = param['mainCategoryID'];
          const subCategoryKey = param['subCategoryID'];
          // TODO Terminar. Agregar un nuevo modelo que contemple al nodo subCategoryBook

          this.categoryService.getMainCategoryByKey(mainCategorykey)
            .then(
              (snap: firebase.database.DataSnapshot) => {
                if (snap.val()) {
                  this.heroImage = snap.val().heroImageUrl;
                  this.subCategoryName = snap.val().subCategories[subCategoryKey].name;
                  this.categoryService.getSubCategoryBookByKey(subCategoryKey)
                    .then(
                      (snapshot: firebase.database.DataSnapshot) => {
                        if (snapshot.val()) {
                          const buildSubCategories = this.categoryService.fillSubCategoryBook(snapshot.val());
                          this.subCategories = new SubCategoryBookModel(subCategoryKey, buildSubCategories);
                          this.isLoading = false;
                        } else {
                          this.isLoading = false;
                          this.errorMessage = `No encontramos ningún libro en ${this.subCategoryName} :(`;
                        }
                      }
                    )
                    .catch(
                      (err: Error) => {
                        this.isLoading = false;
                        console.error(err);
                      }
                    );
                } else {
                  this.errorMessage = 'No encontramos la categoría principal.';
                }
              }
            )
            .catch(
              (err: Error) => {
                this.isLoading = false;
                console.error(err);
              }
            );
        }
      );
  }
}
