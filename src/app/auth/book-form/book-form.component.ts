import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FileManager } from '../file-manager';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../common/services/category.service';
import { CategoryModel } from '../../common/models/category.model';
import { SubCategoryModel } from '../../common/models/sub-category.model';
import { AuthorModel } from '../../common/models/author.model';
import { FormBookService } from '../../common/services/form-book.service';
import { TitleService } from '../../common/services/title.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css'],
  providers: [FileManager]
})
export class BookFormComponent implements OnInit {
  isComponentLoaded = false;
  bookForm: FormGroup;
  categories: CategoryModel[];
  subCategories: SubCategoryModel[];
  showGray = true;

  uploadError: {isError: boolean, title: string, body: string};
  showGenericError = false;
  isSendingForm = false;
  authors: AuthorModel[] = [];
  selectedImage;
  @ViewChild('imageFile') imageControl: ElementRef;
  @ViewChild('imagePreview') imgElement: ElementRef;
  constructor(private fb: FormBuilder,
              private categoryService: CategoryService,
              private formBookService: FormBookService,
              private fileManager: FileManager,
              private titleService: TitleService) {
    this.bookForm = fb.group({
      'book-title' : ['', Validators.compose(
        [Validators.required]
      )],
      'book-edition' : ['', Validators.compose(
        [Validators.min(1), Validators.required]
      )],
      'book-pages' : ['', Validators.compose(
        [Validators.required, Validators.min(10)]
      )],
      'book-release-date' : ['', Validators.required],
      'book-description' : ['', Validators.compose(
        [Validators.required, Validators.maxLength(1300)]
      )],
      'book-main-category' : ['', Validators.required],
      'book-sub-category' : ['', Validators.required]
    });
  }

  ngOnInit() {
    this.formBookService.isSendingForm.asObservable()
      .subscribe(
        (bit: boolean) => {
          this.isSendingForm = bit;
        }
      );
    this.formBookService.errorTryingToUpload.asObservable()
      .subscribe(
        (objectData) => {
          this.uploadError = objectData;
          window.scrollTo(0, 0);
          if (!objectData.isError) {
            this.resetFormAndProperties();
          }
        }
      );
    this.getCategories();
    this.titleService.setTitle('Compartir libro');
  }
  onImageChange() {
    this.selectedImage = null; // Reset the property when the user change the file.
    this.seePreviewImage();
  }
  seePreviewImage() {
    const reader = new FileReader();
    const file: File = this.imageControl.nativeElement.files[0];
    const imgPreview = this.imgElement.nativeElement;
    reader.addEventListener('load', () => {
      imgPreview.src = reader.result;
    });
    reader.readAsDataURL(file);
    if (this.validateImage(file)) {
      this.selectedImage = file;
      this.selectedImage = file;
    }
  }

  validateImage(file: File): boolean {
    // 4MB MAX
    return !this.fileManager.hasPassedMaxBytes(file.size, 4194304)
      && this.fileManager.hasCorrectImageExtention(file.type);
  }

  onFormSubmit() {
    this.uploadError = null;
    this.showGenericError = false;
    if (this.bookForm.valid &&
      this.authors.length > 0 &&
      this.selectedImage &&
      this.formBookService.optionSelected &&
      this.formBookService.optionValue) {
      // Save the book.
      const mainCategoryName = this.findNameMainCategory(this.bookForm.controls['book-main-category'].value);
      const subCategory = this.findNameSubCategory(this.bookForm.controls['book-sub-category'].value);
      this.formBookService.sendBookToFirebaseDatabase(this.bookForm, this.authors,
        this.selectedImage, this.formBookService.optionSelected, this.formBookService.optionValue, mainCategoryName, subCategory);
    } else {
     this.showGenericError = true;
    }
  }
  getCategories() {
    this.categoryService.getAllCategories()
      .then(
      (snapshot: firebase.database.DataSnapshot) => {
        // handle the data
        if (snapshot.exists()) {
          const categories = this.categoryService.orderCategoryAndSubCategory(this.categoryService.fillAllCategories(snapshot));
          this.categories = categories;
          this.isComponentLoaded = true; // Used to show loader
        }
      }
    )
      .catch(
        (err: Error) => {
          console.error(err);
        }
      );
  }
  onMainCategoryChanged() {
    this.bookForm.controls['book-sub-category'].setValue('');
    this.showGray = false;
    const mainCategoryKey = this.bookForm.controls['book-main-category'].value;
    this.categories.find((c) => {
      if (c.key === mainCategoryKey) {
        this.subCategories = c.subCategory;
        return true;
      }
    });
  }
  getAuthorsFromChildComponent(event) {
    // Catch changes from child component (search-author-component)
    this.authors = event;
  }
  onCloseGenericError() {
    this.showGenericError = false;
  }
  findNameMainCategory(key: string): string {
    return this.categories.find((e) => {
      if (e.key === key) {
        return true;
      }
    }).name;
  }
  findNameSubCategory(key: string): string {
    return this.subCategories.find((el) => {
      if (el.key === key) {
        return true;
      }
    }).name;
  }
  resetFormAndProperties() {
    this.bookForm.reset();
    this.authors = [];
    this.selectedImage = null;
    this.imageControl.nativeElement.value = '';
    this.imgElement.nativeElement.src = '';
    this.formBookService.optionValue = '';
    this.formBookService.optionSelected = '';
  }
}
