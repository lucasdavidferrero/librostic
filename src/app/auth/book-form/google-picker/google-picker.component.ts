import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBookService} from '../../../common/services/form-book.service';

declare var $: any;

@Component({
  selector: 'app-google-picker',
  templateUrl: './google-picker.component.html',
  styleUrls: ['./google-picker.component.css']
})
export class GooglePickerComponent implements OnInit, AfterViewInit {
  selectedOption: string;

  hasFileError: boolean;
  errorFileDescription: string;
  @ViewChild('fileControl') fileControl: ElementRef;
  @ViewChild('driveControl') driveControl: ElementRef;
  constructor(private formBookService: FormBookService) { }

  ngOnInit() {
    this.formBookService.errorTryingToUpload.asObservable()
      .subscribe(
        (errTrying) => {
          if (!errTrying.isError) {
            this.resetFields();
          }
        }
      );
  }
  ngAfterViewInit() {
    $('.ui.dropdown.google-picker')
      .dropdown()
    ;
  }
  onSaveClick() {
    this.formBookService.optionSelected = '';
    this.formBookService.optionValue = '';
    this.hasFileError = false;
    switch (this.selectedOption) {
      case 'firebaseStorage':
        const file = this.fileControl.nativeElement.files[0];
        if (file) {
          if (!this.formBookService.checkOptionAndValue('firebaseStorage', this.fileControl.nativeElement.files)) {
            this.hasFileError = true;
            this.errorFileDescription = 'El archivo es demasiado pesado (40MB max) o no es un PDF.';
          }
        } else {
          this.hasFileError = true;
          this.errorFileDescription = 'Por favor selecciona un archivo';
        }
        break;
      case 'googleDrive':
        const driveURL = this.driveControl.nativeElement.value;
        if (driveURL) {
          if (!this.formBookService.checkOptionAndValue('googleDrive', this.driveControl.nativeElement.value)) {
            this.hasFileError = true;
            this.errorFileDescription = 'Al parecer el campo no es correcto.';
          }
        } else {
          this.hasFileError = true;
          this.errorFileDescription = 'Ingresa el ID del archivo Google Drive';
        }
        break;
      default :
        this.hasFileError = true;
        this.errorFileDescription = 'Por favor selecciona un servicio.';
        break;
    }
  }
  resetFields() {
    if (this.fileControl) {
      this.fileControl.nativeElement.value = '';
    }
    if (this.driveControl) {
      this.driveControl.nativeElement.value = '';
    }
  }
}
