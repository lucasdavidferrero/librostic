import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthorModel} from '../../../../common/models/author.model';
import { AuthorService } from '../../../../common/services/author.service';

@Component({
  selector: 'app-query-result',
  templateUrl: './query-result.component.html',
  styleUrls: ['./query-result.component.css']
})
export class QueryResultComponent implements OnInit, OnDestroy {
  @Input() authors: AuthorModel[] = [];
  @Input() isLoading = false;
  constructor(private authorService: AuthorService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
  }
  onAuthorClicked(index) {
    if (!this.checkSelectedAuthor(index)) {
      // everything OK. go ahead
      this.authorService.clickedAuthors.push(this.authors[index]);
      this.authorService.selectedAuthor.next(this.authors[index]);
    } else {
      console.log('This auhtor was already added');
    }
  }

  checkSelectedAuthor(index): boolean {
    return this.authorService.searchByClickedAuthor(this.authors[index]) ? true : false;
  }

}
