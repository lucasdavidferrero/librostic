import {Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import { AuthorModel } from '../../../common/models/author.model';
import { AuthorService } from '../../../common/services/author.service';
import {FormBookService} from "../../../common/services/form-book.service";

@Component({
  selector: 'app-search-author',
  templateUrl: './search-author.component.html',
  styleUrls: ['./search-author.component.css']
})
export class SearchAuthorComponent implements OnInit, OnDestroy {
  @ViewChild('authorNameField') authorNameControl: ElementRef;
  isSearching = false;
  authorsList: AuthorModel[] = [];

  @Output() authorsToBeSaved: EventEmitter<AuthorModel[]> = new EventEmitter<AuthorModel[]>();
  selectedAuthors: AuthorModel[] = [];
  constructor(public authorService: AuthorService,
              private formBookService: FormBookService) { }

  ngOnInit() {
    this.authorService.selectedAuthor.asObservable()
      .subscribe(
        (author: AuthorModel) => {
          this.selectedAuthors.push(author);
          this.authorsToBeSaved.emit(this.selectedAuthors);
        },
        (err) => {
          console.log(err);
        }
      );
    this.formBookService.errorTryingToUpload.asObservable()
      .subscribe(
        (message) => {
          if (!message.isError) {
            // Clear the selected authors.
            this.authorsToBeSaved.emit([]);
            this.selectedAuthors = [];
            this.authorService.clickedAuthors = [];
          }
        }
      );
  }
  ngOnDestroy() {
    this.authorService.authorsFound = [];
    this.authorService.clickedAuthors = [];
  }
  searchAuthor() {
    const authorNameValue = this.authorNameControl.nativeElement.value;
    if (authorNameValue.length > 3) {
      // Search author
      this.isSearching = true;
      const call = this.authorService.searchAuthorByName(authorNameValue);
      call.then(
        () => {
          // Show options.
          this.isSearching = false;
          this.authorsList = this.authorService.authorsFound;
        }
      );
    } else {
      this.authorService.authorsFound = [];
      this.authorsList = [];
    }
  }
  deleteSelectedAuthor(index) {
    this.selectedAuthors.splice(index, 1);
    this.authorsToBeSaved.emit(this.selectedAuthors);
    this.authorService.clickedAuthors.splice(index, 1);

  }
}
