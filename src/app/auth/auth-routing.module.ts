import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserConfigComponent } from './user-config/user-config.component';
import { AddNewAuthorComponent } from './add-new-author/add-new-author.component';
import { BookFormComponent } from './book-form/book-form.component';
import {AuthGuardService} from './auth-guard.service';

const authRoutes: Routes = [
  { path: 'user-config', component: UserConfigComponent, canActivate: [AuthGuardService] },
  { path: 'add-author', component: AddNewAuthorComponent, canActivate: [AuthGuardService] },
  { path: 'new-book', component: BookFormComponent, canActivate: [AuthGuardService]},
];

@NgModule({
  imports: [
    RouterModule.forChild(authRoutes)
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule{}
