import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

import { UserService } from '../common/services/user.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import {AngularFireAuth} from 'angularfire2/auth';

@Injectable()
export class UnloggedGuardService implements CanActivate {
  constructor(private userService: UserService,
              private router: Router,
              private af: AngularFireAuth) {}

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | boolean {

    return this.af.idToken
      .take(1)
      .map((authState) => !!authState)
      .do(authenticated => {
        if (!authenticated){
          this.router.navigate(['']);
          return false;
        }
      });
  }
}
