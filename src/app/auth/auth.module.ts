import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';

import { UserConfigComponent } from './user-config/user-config.component';
import { AddNewAuthorComponent } from './add-new-author/add-new-author.component';
import { BookFormComponent } from './book-form/book-form.component';
import { SearchAuthorComponent } from './book-form/search-author/search-author.component';
import { GooglePickerComponent } from './book-form/google-picker/google-picker.component';
import { QueryResultComponent } from './book-form/search-author/query-result/query-result.component';



@NgModule({
  declarations: [
    UserConfigComponent,
    AddNewAuthorComponent,
    BookFormComponent,
    SearchAuthorComponent,
    GooglePickerComponent,
    QueryResultComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AuthRoutingModule,
    ReactiveFormsModule
  ]
})
export class AuthModule {}
