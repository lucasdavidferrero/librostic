import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import { FileManager } from '../file-manager';
import * as firebase from 'firebase';
import { AuthorService } from '../../common/services/author.service';
import { TitleService } from '../../common/services/title.service';

@Component({
  selector: 'app-add-new-author',
  templateUrl: './add-new-author.component.html',
  styleUrls: ['./add-new-author.component.css'],
  providers: [FileManager]
})
export class AddNewAuthorComponent implements OnInit {
  newAuthorForm: FormGroup;

  isImageTooLarge = false;
  hasImageCorrectFormat = false;
  hasChangeImage = false;
  imageFile: File;

  showErrorMessage = false;
  erroMessageInfo: {title: string, message: string};

  @ViewChild('inputFile') inputControl: ElementRef;
  @ViewChild('imagePreview') previewImage: ElementRef;

  showProgressbar = false;
  @ViewChild('progressBar') progressBar: ElementRef;
  @ViewChild('progressContent') progressContent: ElementRef;
  updatedSuccessfully = false;
  constructor(fb: FormBuilder,
              private fileManager: FileManager,
              private authorService: AuthorService,
              private titleService: TitleService) {

    // FormBuilder
    this.newAuthorForm = fb.group({
      'full-name' : [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(35),
          // TODO apply the pattern Validator so an user only enters letters.
          // TODO FIX THE PATTERN!!
          // Validators.pattern('\w*')
        ])
      ],
      'brief-description' : [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(1500)
        ])
      ]
    });
  }

  ngOnInit() {
    this.resetProgressbar();
    this.titleService.setTitle('nuevo autor');
  }

  onSendNewAuthorForm() {
    console.log(this.newAuthorForm);
    this.resetProgressbar();
    this.updatedSuccessfully = false;
    this.showErrorMessage = false;
    const authorName = this.newAuthorForm.controls['full-name'].value;
    const authorBio = this.newAuthorForm.controls['brief-description'].value;

    if (this.newAuthorForm.valid && this.hasChangeImage && this.hasImageCorrectFormat && !this.isImageTooLarge) {
      // Everything is OK. Send the image and then push to the DB.
      if (this.imageFile) {
        const responseKey = this.authorService.createNewAuthor(authorName.replace(/\s\s+/g, ' '), authorBio);
        if (responseKey) {
          const saveProfileCall = this.authorService.saveProfileImage(this.imageFile, responseKey);
          // Set the progressBar to 100 width. This is the parent of the bar.
          this.progressBar.nativeElement.style.width = '100%';
          saveProfileCall
            .then(
              (res) => {
                this.authorService.updatePorfileImage(responseKey, res.downloadURL)
                  .then(
                    () => {
                      this.updatedSuccessfully = true;
                      this.erroMessageInfo = { title: 'El Autor se agregó correctamente',
                        message: ' Ahora puedes usarlo a la hora de agregar libros.' };
                      this.clearFieldsAndFile();
                    }
                  )
                  .catch(
                    (err: Error) => {
                      // Delete the author and show an error.
                      this.authorService.deleteAuthorByKey('responseKey');
                      this.erroMessageInfo = { title: 'Error inesperado.',
                        message: 'Al parecer ocurrió un error al intentar subir la imágen. Si esto sigue sucediendo, contáctenos. ' +
                        err.name + '. ' + err.message };
                      this.showErrorMessage = true;
                    }
                  );
              }
            );
          saveProfileCall.on('state_changed',
              // progress
              (snapshot: firebase.storage.UploadTaskSnapshot) => {
               const percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
               this.progressContent.nativeElement.style.width = percentage.toFixed(2) + '%';
              },
              // Error
              (err: Error) => {
                this.progressBar.nativeElement.classList.add('error');
              },
              // Completed
              () => {
                this.progressBar.nativeElement.classList.add('success');
              }
              );
        }
      }

    } else if ( !this.hasImageCorrectFormat || this.isImageTooLarge || !this.hasChangeImage  ) {
      this.showErrorMessage = true;
      this.erroMessageInfo = { title : 'Error al intentar subir la imágen.',
                               message: 'Selecciona una imágen válida. ' +
                              'Solo se aceptan formatos PNG, JPG Y JPEG. El tamaño máximo permitido es de 1.5MB' };
    }
  }

  private resetProgressbar() {
    this.showProgressbar = false;
    this.progressBar.nativeElement.style.width = '0';
    this.progressContent.nativeElement.style.width = '0';
    this.progressBar.nativeElement.classList.remove('error', 'success');
  }
  private clearFieldsAndFile() {
    this.newAuthorForm.reset();
    this.imageFile = null;
    this.previewImage.nativeElement.src = '';
    this.inputControl.nativeElement.value = '';
  }
  onChangeImageAuthor() {
    this.seePreviewImage();
    this.hasChangeImage = true;
  }
  private seePreviewImage() {
    this.isImageTooLarge = false;
    const reader = new FileReader();
    const file: File = this.inputControl.nativeElement.files[0];
    const imgPreview = this.previewImage.nativeElement;
    reader.addEventListener('load', () => {
      imgPreview.src = reader.result;
    });
    reader.readAsDataURL(file);
    if (this.fileManager.hasPassedMaxBytes(file.size, 1572864)) {
      this.isImageTooLarge = true;
    }
    this.fileManager.hasCorrectImageExtention(file.type) ? this.hasImageCorrectFormat = true : this.hasImageCorrectFormat = false;
    this.imageFile = file;
  }

  onDismissErrorAuthor() {
    this.showErrorMessage = false;
    this.erroMessageInfo.title = '';
    this.erroMessageInfo.message = '';
    this.updatedSuccessfully = false;
  }
}
