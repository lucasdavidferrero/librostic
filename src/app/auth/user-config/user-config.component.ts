import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../common/services/user.service';
import { AuthService } from '../../common/services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserConnectedService} from '../../common/services/user-connected.service';
import * as firebase from 'firebase';
import {TitleService} from '../../common/services/title.service';


declare var $: any;
@Component({
  selector: 'app-user-config',
  templateUrl: './user-config.component.html',
  styleUrls: ['./user-config.component.css']
})
export class UserConfigComponent implements OnInit {
  isUserInformationFetched = false;
  @ViewChild('showImage') showImage: ElementRef;
  @ViewChild('fileButton') fileButton: ElementRef;
  imageURL: string;
  imageTouched: boolean;
  userEmail: string;
  isImageInvalid: boolean;
  showSizeError = false;
  invalidFileExtection = false;
  configForm: FormGroup;
  nameControl: AbstractControl;
  imageFile: any;
  showSuccessAlert = false;
  showFormError: boolean;
  formErrorContent = {title: '', message: ''};

  isUploadStarted = false;
  errorOnUploadImage = false;
  successfullyUploadOfImage = false;
  @ViewChild('progressElement') progressElement: ElementRef;
  @ViewChild('progressNumber') progressNumber: ElementRef;
  @ViewChild('progressParent') progressParent: ElementRef;

  constructor(private userService: UserService,
              private authService: AuthService,
              private afAuth: AngularFireAuth,
              fb: FormBuilder,
              private userConnectedService: UserConnectedService,
              private titleService: TitleService) {
    // FB stands for Form Builder.
    this.configForm = fb.group({
      'fullname' : ['', Validators.compose([
                        Validators.required,
                        Validators.maxLength(30),
                        Validators.minLength(5)
                    ])]
    });

    this.nameControl = this.configForm.controls['fullname'];
  }

  ngOnInit() {
    // When we get the information from the server then we assing the values of the form
    // and change the state of isUserInformationFetched to show the elements.
    this.afAuth.authState
      .subscribe(
        (data: firebase.User) => {
          if (data) {
            this.nameControl.setValue(data.displayName);
            this.imageURL = data.photoURL;
            this.userEmail = data.email;
            this.isUserInformationFetched = true;
          }
        }
      );
    this.titleService.setTitle('configuración');
  }
  onChangeImage() {
    this.isImageInvalid = false;
    this.showSizeError = false;
    this.invalidFileExtection = false;

    const reader = new FileReader();
    const file: File = this.fileButton.nativeElement.files[0];
    const imageInput = this.showImage.nativeElement;
    reader.addEventListener('load', () => {
      imageInput.src = reader.result;

    });
    reader.readAsDataURL(file);
    if (file.size > 5242880) {
      // The file is greater than 5MB. Don't upload it.
      this.isImageInvalid = true;
      this.showSizeError = true;
    } else {
      // Check if it is an image, if it is OK otherwise set error.
      // Call showFormError and set values to fromErrorContent.
      const typeOfFile = file.type;
      if (typeOfFile === 'image/png' || typeOfFile === 'image/jpeg' || typeOfFile === 'image/jpg'
          || typeOfFile === 'image/gif') {
        this.imageFile = file;
      } else {
        // At this point the file size is bellow 5MB but isn't an image.
        this.invalidFileExtection = true;
        this.isImageInvalid = true;
      }
    }
    this.imageTouched = true;
  }
  onSubmitConfigForm() {
    this.showSuccessAlert = false;
    this.formErrorContent.title = '';
    this.formErrorContent.message = '';
    this.showFormError = false;
    this.isUploadStarted = false;
    this.resetProgressbar();
    // Verificar que el usuario haya cambiado algo en el form
    // Si no lo hizo, mostrar advertencia.
    if (!this.configForm.dirty && !this.imageTouched) {
      this.formErrorContent.title = 'No se detectaron cambios.';
      this.formErrorContent.message = 'Debes cambiar la información de los campos nombre completo o seleccionar una nueva imágen para guardar los cambios.';
      this.showFormError = true;
    } else {
      if (this.configForm.valid && !this.isImageInvalid) {
        // Everything OK. Send the information to the firebaseDatabase and authentication.
        const transformDisplayName = this.nameControl.value.replace(/\s\s+/g, ' ');
        this.userConnectedService.updateDisplayName(this.userService.user.uid, transformDisplayName)
          .then(
            () => {
              if (this.imageFile) {
                const task = this.userConnectedService.updateProfilePhoto( this.userService.user.uid, this.imageFile );
                this.isUploadStarted = true;
                this.showProgressBar(task);
                  task.then(
                    (res) => {
                      firebase.auth().currentUser.updateProfile({displayName: transformDisplayName, photoURL: res.downloadURL });
                      this.userService.user.photoUrl = res.downloadURL;
                      this.userService.user.displayName = transformDisplayName;
                      this.userConnectedService.changeProfileURL(this.userService.user.uid, res.downloadURL);
                    }
                  )
                  .catch(
                    (err) => {
                      console.error('An error happend when trying to upload the photo.', err);
                    }
                  );
              } else {
                firebase.auth().currentUser.updateProfile({displayName: transformDisplayName, photoURL: this.userService.user.photoUrl });
                this.userService.user.displayName = transformDisplayName;
                this.showSuccessAlert = true;
              }
            }
          );

      } else if (this.invalidFileExtection) {
        this.formErrorContent.title = 'El archivo de imágen es inválido.';
        this.formErrorContent.message = 'Solo se aceptan archivos de imágenes jpg, png, jpeg y gifs.';
        this.showFormError = true;
      } else {
        this.formErrorContent.title = 'Soluciona los errores marcados.';
        this.formErrorContent.message = 'Para poder actualizar tu información debes de solucionar los errores marcados que están debajo.';
        this.showFormError = true;
      }
    }
  }

  private showProgressBar(task: firebase.storage.UploadTask) {
    task.on('state_changed',
      // Progress
      (snapshot: firebase.storage.UploadTaskSnapshot) => {
        let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        this.progressNumber.nativeElement.textContent = percentage.toFixed(2) + '%';
        this.progressElement.nativeElement.style.width = percentage.toFixed(2) + '%';
      },
      // Error
      (err) => {
        this.errorOnUploadImage = true;
        this.formErrorContent = { title: 'Ocurrió un error inesperado al intertar subir tu imágen.',
                                  message: 'Error: ' + err.message + '. Si esto sigue sucediendo contáctenos enviandonos el error.' };

        this.showFormError = true;

      },
      // Complete
      () => {
        this.progressParent.nativeElement.classList.add('success');
        this.successfullyUploadOfImage = true;
        this.showSuccessAlert = true;
      }
      );
  }

  private resetProgressbar() {
    this.progressNumber.nativeElement.textContent = '0%';
    this.progressElement.nativeElement.style.width = '';
    this.progressParent.nativeElement.classList.remove('success');
    this.successfullyUploadOfImage = false;
    this.errorOnUploadImage = false;
}

  onCloseFormConfigAlert() {
    this.showFormError = false;
  }
}
