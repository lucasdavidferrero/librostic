export class FileManager {
  hasCorrectImageExtention(fileType: string): boolean {
    switch (fileType) {
      case 'image/png':
        return true;
      case 'image/jpg':
        return true;
      case 'image/jpeg':
        return true;
      default:
        return false;
    }
  }
  hasPassedMaxBytes(fileSize: number, maxBytesAllowed: number): boolean {
    return fileSize > maxBytesAllowed;
  }

  isPdf(fileType: string): boolean {
    return fileType === 'application/pdf';
  }
}
