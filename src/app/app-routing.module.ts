import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './shared/home/home.component';
import { LoginComponent } from './public/login/login.component';
import { BookDetailComponent } from './shared/book-detail/book-detail.component';
import { SearchResultComponent } from './shared/search-result/search-result.component';
import { SearchNotFoundComponent } from './shared/search-result/search-not-found/search-not-found.component';
import { CategoryListComponent } from './shared/categories/category-list/category-list.component';
import { CategoryDetailComponent } from './shared/categories/category-detail/category-detail.component';
import { AuthorComponent } from './shared/author/author.component';
import {NotFoundComponent} from './shared/not-found/not-found.component';
import {AdminGuardService} from './+admin/admin-guard.service';

const appRoutes: Routes = [
  { path: '', component: HomeComponent , pathMatch: 'full' },
  { path: 'dashboard', loadChildren: './+admin/admin.module#AdminModule', canLoad: [AdminGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'book/:key/:name', component: BookDetailComponent },
  { path: 'search/:option/:query', component: SearchResultComponent },
  { path: 'categories', component: CategoryListComponent },
  { path: 'category/:mainCategoryID/:subCategoryID/:subCategoryName', component: CategoryDetailComponent },
  { path: 'author/:authorID/:authorName', component: AuthorComponent },
  { path: 'book-not-found', component: SearchNotFoundComponent },
  { path: '**', component: NotFoundComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
