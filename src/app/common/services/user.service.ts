import { Injectable } from '@angular/core';
import { UserModel } from '../models/user.model';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import {AngularFireAuth} from 'angularfire2/auth';

@Injectable()
export class UserService {
  private _user: UserModel;
  private _isUserAdmin: Subject<boolean> = new Subject();
  private _token: string;
  isAdmin: boolean;
  private userRef = firebase.database().ref('users');
  constructor(private af: AngularFireAuth) {}

  get user(): UserModel {
    return this._user;
  }
  set user(user: UserModel) {
    this._user = user;
  }


  get isUserAdmin(): Observable<boolean> {
    return this._isUserAdmin.asObservable();
  }

  setIsUserAdmin(bit: boolean) {
    this._isUserAdmin.next(bit);
  }

  isUserAdminOrMod() {
    const loggedUID = firebase.auth().currentUser.uid;
    const adminRef = firebase.database().ref(`admin/${loggedUID}`);
    const modRef = firebase.database().ref(`mod/${loggedUID}`);
    if (loggedUID) {
      adminRef.once('value')
        .then(
          (snap: firebase.database.DataSnapshot) => {
            if (snap.exists()) {
            }
          }
        );
    } else {
      return false;
    }

  }

  getUserByKey(key: string): Promise<any> {
    return this.userRef.child(key).once('value');
  }

}
