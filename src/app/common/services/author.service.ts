import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { UserService } from './user.service';
import { AuthorModel } from '../models/author.model';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class AuthorService {
  authorsFound: AuthorModel[] = [];
  clickedAuthors: AuthorModel[] = []; // With this we verify if an author is already selected.
  selectedAuthor: Subject<AuthorModel> = new Subject();

  constructor(private userService: UserService) {}

  createNewAuthor(name: string, bio: string): string {
    const queryableName = name.replace(/\s+/g, '-').toLowerCase();
    const dbRef = firebase.database().ref('authors/');
    return dbRef.push(
      {
        name: name,
        briefBio: bio,
        uploadedUser: this.userService.user.uid,
        uploadedDate: new Date().toJSON(),
        queryableName: queryableName
      }
    ).key;
  }

  updatePorfileImage(key: string, imageURL: string): Promise<any> {
    const dbRef = firebase.database().ref(`authors/${key}`);
    return dbRef.update(
      {
        photoURL: imageURL
      }
    );
  }

  deleteAuthorByKey(key: string): Promise<any> {
    const dbRef = firebase.database().ref(`authors/${key}`);
    return dbRef.remove();
  }

  saveProfileImage(file: File, key: string): firebase.storage.UploadTask {
    const storageRef = firebase.storage().ref(`author/${key}/author-profile`);
    return storageRef.put(file);
  }

  searchAuthorByName(nameAuthor: string): Promise<any> {
    const transformedNameAuthor = nameAuthor.replace(/\s+/g, '-').toLowerCase();
    this.authorsFound = []; // Reset the founded authors.
    const authors: AuthorModel[] = [];
    const storageRef = firebase.database().ref(`authors/`);
    return storageRef.orderByChild('queryableName').startAt(transformedNameAuthor).endAt(transformedNameAuthor + '\uf8ff').limitToFirst(7)
      .once('value',
      (snapshot: firebase.database.DataSnapshot) => {
        if (snapshot.exists()) {
          const authorResponse = snapshot.val();
          const keys = Object.keys(authorResponse);
          for (let i = 0; i < keys.length; i++) {
            const k = keys[i];
            authors.push(new AuthorModel(authorResponse[k].name, authorResponse[k].briefBio,
                        k, authorResponse[k].photoURL));
          }
        }
        this.authorsFound = authors;
      },
      (err) => {
        console.error(err);
      }
    );
  }

  searchByClickedAuthor(author: AuthorModel) {
    return this.clickedAuthors.find(value => value.$key === author.$key);
  }

  getAuthorByKey(authorKey: string): Promise<any> {
    const authorRef = firebase.database().ref(`authors/${authorKey}`);
    return authorRef.once('value');
  }

  fillAuthorModel(data, authorKey: string): AuthorModel {
    const author = new AuthorModel(data.name, data.briefBio, authorKey, data.photoURL);
    return author;
  }

  fillAuthorBooks(books): {bookKey: string, bookTitle?: string, bookPhoto: string}[]{
    const buildBooks: {bookKey: string, bookTitle?: string, bookPhoto: string}[] = [];
    const keys = Object.keys(books);
    for (let i = 0; i < keys.length; i++) {
      const k = keys[i];
      buildBooks.push({bookKey: k, bookTitle: books[k].title, bookPhoto: books[k].coverImage});
    }
    return buildBooks;
  }
}
