import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { UserService } from '../user.service';

@Injectable()
export class BanUserService {
  dbRef = firebase.database().ref();
  isStaff: boolean;
  constructor(private userService: UserService) {}
  checkIfTheUserIsStaff(key: string): Promise<any> {
    this.isStaff = false;
    // See the nodes admin and mod
    return this.dbRef.child(`admin/${key}`).once('value')
      .then(
        (snap: firebase.database.DataSnapshot) => {
          if (!snap.val()) {
            this.dbRef.child(`mod/${key}`).once('value')
              .then(
                (snapshot: firebase.database.DataSnapshot) => {
                  if (snapshot.val()) {
                    this.isStaff = true;
                  }
                }
              );
          } else {
            this.isStaff = true;
          }
        }
      );
  }

  userExist(key: string): Promise<any> {
    return this.dbRef.child(`users/${key}`).once('value');
  }

  blockUser(key: string, userEmail: string, reason: string): firebase.database.ThenableReference {
    return this.dbRef.child(`Banned`).push({
      date: new Date(),
      email: userEmail,
      reason: reason,
      responsable: this.userService.user.uid,
      user: key
    });
  }
  changeIsBannedToTrue(key: string, banKey: string): Promise<any> {
    return this.dbRef.child(`users/${key}`)
      .update({
        isBanned: banKey
      });
  }
}
