import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { UserModel } from '../../models/user.model';

@Injectable()
export class DashboardUsersService {
  userNodeRef = firebase.database().ref('users');
  usersFound: UserModel[];
  constructor() {}

  filterListUser(typeOfSearch: string, lastKey?: string) {
    switch (typeOfSearch) {
      case 'mostRecent':
        // Call the function that get users by recent date.
        // TODO De momento dejamos esta opción. En futuras versiones podríamos mejorar esto.
        return this.fillUsers(this.getRecentUsers(lastKey));
      default:
        break;
    }
  }
  getRecentUsers(lastKey?: string): Promise<any> {
    if (lastKey) {
      return this.userNodeRef.orderByKey().startAt(lastKey).limitToFirst(12).once('value');
    }
    return this.userNodeRef.limitToFirst(12).once('value');
  }
  // TODO MEJORAR PAGINACIÓN!!
  private fillUsers(responseUser: Promise<any>): Promise<any> {
    const users: UserModel[] = [];
    return responseUser
      .then(
        (snapshot: firebase.database.DataSnapshot) => {
          if (snapshot.val()) {
            const data = snapshot.val();
            const keys = Object.keys(data);
            for (let i = 0; i < keys.length; i++) {
              const k = keys[i];
              users.push(new UserModel(k, data[k].displayName, data[k].email,
                data[k].photoURL, data[k].providerId));
            }
            this.usersFound = users;
          }
        }
      )
      .catch(
        (err: Error) => {
          console.error(err);
        }
      );
  }


  // Ban users
}
