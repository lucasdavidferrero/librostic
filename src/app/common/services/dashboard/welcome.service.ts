import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
@Injectable()
export class WelcomeService {
  constructor() {}
  getStadistics(): Promise<any> {
    const stadisticRef = firebase.database().ref('stadistic');
    return stadisticRef.once('value');
  }
}
