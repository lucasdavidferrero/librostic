import { Injectable} from '@angular/core';
import * as firebase from 'firebase';
import { CategoryModel } from '../models/category.model';
import { SubCategoryModel } from '../models/sub-category.model';

@Injectable()
export class CategoryService {
  private categoryRef: firebase.database.Reference = firebase.database().ref('category');
  constructor() {}

  addMainCategory(name: string): string {
    return this.categoryRef.push({
      name : name
    }).key;
  }
  getAllMainCategories(): Promise<any> {
    return this.categoryRef.orderByChild('name').once('value');
  }
  fillCategoryModel(snapshot: firebase.database.DataSnapshot) {
    let buildCategory: CategoryModel[] = [];
    if (snapshot.exists()) {
      const response = snapshot.val();
      const key = Object.keys(response);
      for (let i = 0; i < key.length; i++) {
        const k = key[i];
        buildCategory.push(new CategoryModel(k, response[k].name, response[k].heroImageUrl));
      }
      return buildCategory;
    }
  }

  uploadMainCategoryHeroImage(file: File, key: string): Promise<any> {
    const storageRef = firebase.storage().ref(`category/${key}/heroImage`);
    const categoryRefKey = firebase.database().ref(`category/${key}`);
    return storageRef.put(file)
      .then(
        (res) => {
          categoryRefKey.update({
            heroImageUrl: res.downloadURL
          });
        }
      )
      .catch(
        (err) => {
          categoryRefKey.remove();
        }
      );
  }

  addNewSubcategory(mainCategoryKey: string, subCategoryName: string): firebase.database.ThenableReference {
    return this.categoryRef.child(mainCategoryKey).child('subCategories').push({
      name: subCategoryName,
      bookCounter : 0
    });
  }
  getAllCategories() {
    return this.categoryRef.once('value');
  }
  fillAllCategories(snapshot: firebase.database.DataSnapshot): CategoryModel[] {
    const categories: CategoryModel[] = [];
    let subCategories: SubCategoryModel[] = [];
    const data = snapshot.val();
    const keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      const k = keys[i];

      const subCategoryKeys = Object.keys(data[k].subCategories);
      const subData = data[k];
      subCategories = [];
        for (let j = 0; j < subCategoryKeys.length; j++) {
          const sk = subCategoryKeys[j];
          subCategories.push(new SubCategoryModel(sk, subData.subCategories[sk].name, subData.subCategories[sk].bookCounter));
        }
      categories.push(new CategoryModel(k, data[k].name, data[k].heroImageUrl, subCategories));
    }
    return categories;
  }
  orderCategoryAndSubCategory(categories: CategoryModel[]): CategoryModel[] {
    const keys = Object.keys(categories);
    for (let i = 0; i < keys.length; i++) {
      const k = keys[i];
      categories[k].subCategory.sort((a, b) => {
        if (a.name < b.name) { return -1; }
        if (a.name > b.name) { return 1; }
        return 0;
      });
    }
    categories.sort((a, b) => {
      if (a.name < b.name) { return -1; }
      if (a.name > b.name) { return 1; }
      return 0;
    });
    return categories;
  }

  getMainCategoryByKey(mainCategory: string): Promise<any> {
    return this.categoryRef.child(mainCategory).once('value');
  }

  getSubCategoryBookByKey(subCategoryKey: string): Promise<any> {
    const subCategoryBook = firebase.database().ref('subCategoryBook');
    return subCategoryBook.child(subCategoryKey).once('value');
  }

  fillSubCategoryBook(data): {key: string, coverImage: string, title?: string}[] {
    // We get the book's node from subCategoryBook
    const objects = [];
    const keys = Object.keys(data.books);
    for (let i = 0; i < keys.length; i++) {
      const k = keys[i];
      objects.push({key: k, coverImage: data.books[k].coverImage, title: data.books[k].title});
    }
    return objects;
  }
}
