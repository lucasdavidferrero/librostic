import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import {BookModel} from '../models/book.model';
import { AuthorMetaModel } from '../models/author-meta.model';
import { CategoryMetaModel } from '../models/category-meta.model';

@Injectable()
export class BookService {
  constructor(private http: Http) {}

  getShallowBooks() {

    return this.http.get(`https://librostic-fa290.firebaseio.com/books.json?&shallow=true`)
      .map(
        (res: Response) => {
          const data = res.json();
          return Object.keys(data);
        }
      );
  }

  getLazyBooks(lastKey?): Promise<any> {
    const bookRef = firebase.database().ref('books');
    if (!lastKey) {
      return bookRef.orderByKey().limitToLast(20).once('value');
    }
    return bookRef.orderByKey().endAt(lastKey).limitToLast(20).once('value');
  }

  getFirstestBooks() {
    const bookRef = firebase.database().ref('books');
    return bookRef.orderByKey().limitToFirst(2).once('value');
  }

  getBookByKey(key: string): Promise<any> {
    return firebase.database().ref(`books/${key}`).once('value');
  }

  fillBookObject(snap: firebase.database.DataSnapshot): BookModel {
    const authors: AuthorMetaModel[] = [];
    const data = snap.val();
    const authorsData = data.authors;
    const authorKeys = Object.keys(authorsData);
    for (let i = 0; i < authorKeys.length; i++) {
      const k = authorKeys[i];
      authors.push(new AuthorMetaModel(k, authorsData[k].authorName));
    }
    const categories = new CategoryMetaModel(
      {key: data.category.mainCategory.keyMainCategory, name: data.category.mainCategory.name},
      {key: data.category.subCategory.keySubCategory, name: data.category.subCategory.name});
    const book = new BookModel(snap.key, authors, data.briefDescription, categories, data.coverImage,
      data.downloadOption, data.downloadURL, data.edition, data.likes, data.numberOfPages, data.releaseDate, data.title,
      data.uploadedUser);
    return book;
  }

  convertObjectsIntoBooks(data): BookModel[] {
    const books: BookModel[] = [];
    const keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      const k = keys[i];
      const authors: AuthorMetaModel[] = [];
      const authorKeys = Object.keys(data[k].authors);
      for (let j = 0; j < authorKeys.length; j++) {
        const authorK = authorKeys[j];
        authors.push(new AuthorMetaModel(authorK, data[k].authors[authorK].authorName));
      }
      books.push( new BookModel(k, authors, data[k].briefDescription,
        new CategoryMetaModel({key: data[k].category.mainCategory.keyMainCategory,
            name: data[k].category.mainCategory.name},
          {key: data[k].category.subCategory.keySubCategory,
            name: data[k].category.subCategory.name}), data[k].coverImage, data[k].downloadOption, data[k].downloadURL,
        data[k].edition, data[k].likes, data[k].numberOfPages, data[k].releaseDate,
        data[k].title, data[k].uploadedUser) );
    }
    return books;
  }

  updateLinkToGoogleDrive(newDriveKey: string, bookKey: string): Promise<any> {
    const bookRef = firebase.database().ref(`books/${bookKey}`);
    return bookRef.update(
      {
        downloadOption: 'googleDrive',
        downloadURL: newDriveKey.trim()
      }
    );
  }

  updateCoverImageURL(newURL: string, bookKey: string): Promise<any> {
    const bookRef = firebase.database().ref(`books/${bookKey}`);
    return bookRef.update(
      {
        coverImage: newURL
      }
    );
  }


  // ### DELETE AND DECREMENT FUNCTIONS ###

  deleteBook(bookID: string): Promise<any> {
    const bookRef = firebase.database().ref(`books/${bookID}`);
    return bookRef.remove();
  }

  deleteImageFromStorage(bookID: string): Promise<any> {
    // Here we delete both the cover image and the PDF file (if it exist).
    const storageRef = firebase.storage().ref(`books/${bookID}/coverImage`);
    return storageRef.delete();
  }

  deletePDFfromStorage(bookID: string) {
    const storageRef = firebase.storage().ref(`books/${bookID}/bookPDF`);
    return storageRef.delete();
  }

  deleteBookFromSubCategory(subCategoryKey: string, bookID: string): Promise<any> {
    const subRef = firebase.database().ref(`subCategoryBook/${subCategoryKey}/books/${bookID}`);
    return subRef.remove();
  }

  deleteBookFromAuthor(authorKey: string, bookID: string): Promise<any> {
    const authorRef = firebase.database().ref(`authors/${authorKey}/books/${bookID}`);
    return authorRef.remove();
  }

  decrementSubCategoryCounter(mainCategory: string, subCategory: string): Promise<any> {
    const categoryRef = firebase.database().ref(`category/${mainCategory}/subCategories/${subCategory}/bookCounter`);
    return categoryRef.transaction(
      (bookCounter) => {
        bookCounter -= 1;
        return bookCounter;
      }
    );
  }

  decrementUploadedBooks(): Promise<any> {
    const stadisticRef = firebase.database().ref('stadistic/uploadedBooks');
    return stadisticRef.transaction(
      (uploadedBooks) => {
        uploadedBooks -= 1;
        return uploadedBooks;
      }
    );
  }

  // This method gets the password from a confidential node. A node that just
  // admins can read and no body should write.
  allowAction(givenPassword: string): Promise<any> {
    const confidentialNode = firebase.database().ref('app/confidential/masterPassword');
    return confidentialNode.once('value');
  }
}
