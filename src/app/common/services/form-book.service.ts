import { Injectable } from '@angular/core';
import { FileManager } from '../../auth/file-manager';
import { FormGroup } from '@angular/forms';
import { AuthorModel } from '../models/author.model';
import * as firebase from 'firebase';
import {UserService} from './user.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class FormBookService {
  firebaseDbRef = firebase.database().ref('books');
  firebaseDbErrorsRef = firebase.database().ref('errors');

  errorTryingToUpload: Subject<{isError: boolean, title: string, body: string}> = new Subject();
  isSendingForm: Subject<boolean> = new Subject();

  optionSelected: string;
  optionValue: string |File;
  constructor(private fileManager: FileManager,
              private userService: UserService) {}
  checkOptionAndValue(optionSelected: string, optionValue: string | File): boolean {
    switch (optionSelected) {
      case 'firebaseStorage':
        if (this.fileManager.isPdf(optionValue[0].type)
            // 40 MB MAX
          && !this.fileManager.hasPassedMaxBytes(optionValue[0].size, 41943040)) {
          this.optionSelected = 'firebaseStorage';
          this.optionValue = optionValue[0];
          return true;
        } else {
          return false;
        }
      case 'googleDrive':
        // We store the Google Drive's ID
        this.optionSelected = 'googleDrive';
        this.optionValue = optionValue;
        return true;
      default :
        return false;
    }
  }

  sendBookToFirebaseDatabase(bookForm: FormGroup, authors: AuthorModel[], image: File,
                             optionSelected: string, optionValue: string | File,
                             mainCategoryName: string, subCategory: string) {
    this.isSendingForm.next(true);
    let downloadImageUrl: string;
    let pdfURL: string;
    let googleDriveUID: string | File;
    // first: Save the info on books.
    this.pushBasicInformationOfTheBook(bookForm, authors, mainCategoryName, subCategory)
      .then(
      (res: firebase.database.ThenableReference) => {
        // Now save the image, after that save the PDF if is provided and finally update Authors(books and counter),
        // categories(books and counter) and stadistics.
        this.saveCoverImage(image, res.key)
          .then(
            (taskSnapshot: firebase.storage.UploadTaskSnapshot) => {
              downloadImageUrl = taskSnapshot.downloadURL;
              this.firebaseDbRef.child(res.key)
                .update({
                  coverImage: downloadImageUrl
                });
              // Ask if optionSelected is firebase, if it is, store in fb Storage.
              if (optionSelected === 'firebaseStorage') {
                this.saveFilePDF(optionValue, res.key)
                  .then(
                    (taskPDF: firebase.storage.UploadTaskSnapshot) => {
                      pdfURL = taskPDF.downloadURL;
                      this.firebaseDbRef.child(res.key).update(
                        {
                          downloadOption: 'firebaseStorage',
                          downloadURL: pdfURL
                        }
                      );
                    }
                  )
                  .catch(
                    (errPDF: Error) => {
                      this.isSendingForm.next(false);
                      this.deleteBookByKey(res.key)
                        .then(
                          () => {
                            this.firebaseDbErrorsRef.push(
                              {
                                name: errPDF.name,
                                message: errPDF.message,
                                stack: errPDF.stack,
                                description: 'This error was generated when the user was trying to upload the PDF of a book.',
                                triggeredUserID: this.userService.user.uid,
                                triggeredUserEmail: this.userService.user.email,
                              }
                            ).then(
                              () => {
                                this.errorTryingToUpload.next(
                                  {
                                    isError: true,
                                    title: 'Ocurrió un error al intentar subir el archivo PDF',
                                    body: 'Por favor, verifique que el archivo que intenta subir es un PDF no excede el tamaño máximo.'
                                  }
                                );
                              }
                            );
                          }
                        ).then(
                        () => {
                          this.deleteImageByKey(res.key);
                        }
                      );
                    }
                  );

              } else if (optionSelected === 'googleDrive') {
                googleDriveUID = optionValue;
                this.firebaseDbRef.child(res.key).update(
                  {
                    downloadOption: 'googleDrive',
                    downloadURL: googleDriveUID
                  }
                );
              }
              // Update authors, subCategories, subCategoriesBooks(save the book) and stadistics.
              this.updateRelatedNodes(bookForm, authors, res.key, downloadImageUrl);
            }
          )
          .catch(
            (errTask: Error) => {
              this.isSendingForm.next(false);
              // Send to error's node.
              this.firebaseDbErrorsRef.push({
                name: errTask.name,
                message: errTask.message,
                stack: errTask.stack,
                description: 'This error was generated when the user was trying to upload the cover image of a book.',
                triggeredUserID: this.userService.user.uid,
                triggeredUserEmail: this.userService.user.email,
              }).then(
                () => {
                  this.deleteBookByKey(res.key)
                    .then(
                      () => {
                        this.errorTryingToUpload.next({
                          isError: true,
                          title: 'Ocurrió un error al intentar subir la imágen.',
                          body: 'Por favor, verifique que la imágen sea correcta.'
                        });
                      }
                    );
                }
              );
            }
          );
      }
    );
  }
  pushBasicInformationOfTheBook(bookForm: FormGroup, authorsRelated: AuthorModel[],
                                mainCategoryName, subCategory): firebase.database.ThenableReference {
    const authors = {};
    for (let i = 0; i < authorsRelated.length; i++) {
      authors[authorsRelated[i].$key] = { authorName: authorsRelated[i].fullName };
    }
    return this.firebaseDbRef.push(
      {
        title: bookForm.controls['book-title'].value,
        queryableTitle: bookForm.controls['book-title'].value.replace(/\s+/g, '-').toLowerCase(),
        briefDescription: bookForm.controls['book-description'].value,
        numberOfPages: bookForm.controls['book-pages'].value,
        edition: bookForm.controls['book-edition'].value,
        releaseDate: bookForm.controls['book-release-date'].value,
        authors,
        category : {
          mainCategory: {
            keyMainCategory: bookForm.controls['book-main-category'].value, name: mainCategoryName,
          },
          subCategory: {
            keySubCategory: bookForm.controls['book-sub-category'].value, name: subCategory
          }
        },
        uploadedUser: this.userService.user.uid,
        likes: 0
      }
    );
  }
  updateRelatedNodes(bookForm: FormGroup, authors: AuthorModel[], bookKey: string, bookImageURL: string) {
    // UpdateAuthors
    for (let i = 0; i < authors.length; i++) {
      this.addBookToAuthor(authors[i].$key, bookKey, bookImageURL, bookForm.controls['book-title'].value.replace(/\s+/g, '-').toLowerCase());
    }
    // Update bookCounter on subCategory
    this.incrementSubcategoryBookCounter(bookForm.controls['book-sub-category'].value, bookForm.controls['book-main-category'].value);

    // Update SubcategoryBook (add the book)
    this.addNewBookToSubcategyAndBookNode(bookForm.controls['book-sub-category'].value,
      bookKey, bookImageURL, bookForm.controls['book-title'].value.replace(/\s+/g, '-').toLowerCase());

    // Increment uploaded books on stadistics
    this.incrementUploadedBooksStadistic()
      .then(
        () => {
          this.isSendingForm.next(false);
          this.errorTryingToUpload.next({isError: false, title: 'Libro agregado satisfactoriamente',
            body: `El libro ${bookForm.controls['book-title'].value} fue agregado correctamente.`});
        }
      );
  }
  saveCoverImage(file: File, key: string): firebase.storage.UploadTask {
    // TODO Remember to set the RULES on Firebase Storage!
    const fbStorage = firebase.storage().ref(`books/${key}/coverImage`);
    return fbStorage.put(file);
  }
  saveFilePDF(file: string | File, key: string): firebase.storage.UploadTask {
    const fbStorageRef = firebase.storage().ref(`books/${key}/bookPDF`);
    return fbStorageRef.put(file);
  }
  deleteBookByKey(key: string): Promise<any> {
    return this.firebaseDbRef.child(key).remove();
  }
  deleteImageByKey(key: string): Promise<any> {
    const fbStorageRef = firebase.storage().ref(`books/${key}/coverImage`);
    return fbStorageRef.delete();
  }
  addBookToAuthor(authorKey: string, bookKey: string, coverImageBookURL: string, bookTitle: string) {
    const authorRef = firebase.database().ref(`authors/${authorKey}/books`);
    authorRef.update({
      [bookKey] : {
        coverImage: coverImageBookURL,
        title: bookTitle
      }
    });
  }
  incrementSubcategoryBookCounter(subCategoryKey: string, mainCategoryKey: string) {
    const subCategoryRef = firebase.database().ref(`category/${mainCategoryKey}/subCategories/${subCategoryKey}/bookCounter`);
    subCategoryRef.transaction(
      (bookCounter) => {
        bookCounter += 1;
        return bookCounter;
      }
    );
  }
  addNewBookToSubcategyAndBookNode(keySubcategory: string, bookKey: string, bookImageURL: string, queryableName: string) {
    const subCategoryAndBookRef = firebase.database().ref(`subCategoryBook/${keySubcategory}/books`);
    subCategoryAndBookRef.update({
      [bookKey] : {
        coverImage: bookImageURL,
        title: queryableName
      }
    });
  }
  incrementUploadedBooksStadistic(): Promise<any> {
    const stadisticRef = firebase.database().ref('stadistic/uploadedBooks');
    return stadisticRef.transaction(
      (uploadedBooks) => {
        uploadedBooks += 1;
        return uploadedBooks;
      }
    );
  }
}

