import * as firebase from 'firebase';

export class LikeBookService {
  likeRef = firebase.database().ref('bookUserLike');

  constructor() {}

  saveLike(bookID: string, userID: string) {
    this.checkIfUserAlreadyLikeBook(bookID, userID)
      .then(
        (snap: firebase.database.DataSnapshot) => {
          if (!snap.exists()) {
            const likeBookRef = snap.ref.parent;

            likeBookRef.update(
              {
                [bookID] : true
              }
            ).then(
              () => {
                likeBookRef.parent.child('counter').transaction(
                  (counterLikes) => {
                    counterLikes += 1;
                    return counterLikes;
                  }
                );
              }
              // Update the total likes on the book.
            ).then(
              () => {
                const bookRef = firebase.database().ref(`books/${bookID}/likes`);
                bookRef.transaction(
                  (bookLikes) => {
                    bookLikes += 1;
                    return bookLikes;
                  }
                );
              }
            );
          } else {
            // Do the opposite... Erase like and decrement the counter.
            const likeRef = snap.ref;
            likeRef.remove()
              .then(
                () => {
                  const counterRef = likeRef.parent.parent.child('counter');

                  counterRef.transaction(
                    (counterLikes) => {
                      counterLikes -= 1;
                      return counterLikes;
                    }
                  );
                }
              ).then(
              () => {
                const bookRef = firebase.database().ref(`books/${bookID}/likes`);
                bookRef.transaction(
                  (bookLikes) => {
                    bookLikes -= 1;
                    return bookLikes;
                  }
                );
              }
            );
          }
        }
      );
  }

  checkIfUserAlreadyLikeBook(bookID: string, userID: string): Promise<any> {
    return this.likeRef.child(`${userID}/books/${bookID}`).once('value');
  }

}
