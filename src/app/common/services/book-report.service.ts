import * as firebase from 'firebase';
import { ReportBookModel } from '../models/report-book.model';
import {ReportBookSubjectModel} from '../models/report-book-subject.model';

export class BookReportService {
  private reportRef = firebase.database().ref('bookReports');
  constructor() { }

  checkReport(bookID: string, reportedOption: string, userID: string, userName: string): Promise<any> | null {
    const reportBookRef = this.setRefToSelectedOption(reportedOption, bookID);
    if (reportBookRef !== null) {
      return reportBookRef.child(`users/${userID}`).once('value');
    }
    return null;
  }

  pushReport(ref: firebase.database.Reference): Promise<any> {
    const userID = ref.key;
    const parentRef = ref.parent;

    return parentRef.update(
      {
        [userID]: true
      }
    );
  }

  incrementReportCounter(ref: firebase.database.Reference) {
    const parentRef = ref.parent.parent.child('counter');
    const bookCounter = ref.parent.parent.parent.child('totalCounter');
    const stadisticCounter = firebase.database().ref('stadistic/totalReports');
    // Increment the counter in the given node and in the stadistic's node. We don't catch the error
    // because it's not relevant.
    parentRef.transaction(
      (reportCounter) => {
        reportCounter += 1;
        return reportCounter;
      }
    ).then(
      () => {
        bookCounter.transaction(
          (counter) => {
            counter += 1;
            return counter;
          }
        );
      }
    ).then(
      () => {
        stadisticCounter.transaction(
          (totalReports) => {
            totalReports += 1;
            return totalReports;
          }
        );
      }
    );
  }

  // Here we check the selected option to see what kind of report is and also to set
  // the reference properly.
  private setRefToSelectedOption(reportedOption: string, bookID: string): firebase.database.Reference | null {
    let refResult;
    switch (reportedOption) {
      case 'link':
        refResult = this.reportRef.child(`${bookID}/link`);
        break;
      case 'wrongImage':
        refResult = this.reportRef.child(`${bookID}/wrongImage`);
        break;
      case 'largeFile':
        refResult = this.reportRef.child(`${bookID}/largeFile`);
        break;
      default:
        return null;
    }
    return refResult;
    // At this time the ref look like this: keybook-->(link | wrongImage | largeFile)
  }


  // DASHBOARD STUFF

  get20MostReported(): Promise<any> {
    const NUMBER_OF_REPORTS = 20;
    return this.reportRef.orderByChild('totalCounter').limitToFirst(NUMBER_OF_REPORTS).once('value');
  }

  transformResponseToArray(data): ReportBookModel[] {
    const reports: ReportBookModel[] = [];
    let links: ReportBookSubjectModel = null;
    let largeFile: ReportBookSubjectModel  = null;
    let wrongImage: ReportBookSubjectModel = null;

    const keys = Object.keys(data);
    for (let i = 0; i < keys.length; i ++) {
      const k = keys[i];

      // Links
      if (data[k].link) {
        const linksUser = Object.keys(data[k].link.users);
        links = new ReportBookSubjectModel('link', data[k].link.counter, linksUser);
      } else {
        links = null;
      }

      // Large File
      if (data[k].largeFile) {
        const largeFileUser = Object.keys(data[k].largeFile.users);
        largeFile = new ReportBookSubjectModel('largeFile', data[k].largeFile.counter, largeFileUser);
      } else {
        largeFile = null;
      }

      // Wrong Image
      if (data[k].wrongImage) {
        const wrongImageUser = Object.keys(data[k].wrongImage.users);
        wrongImage = new ReportBookSubjectModel('wrongImage', data[k].wrongImage.counter, wrongImageUser);
      } else {
        wrongImage = null;
      }

      const currentReport = new ReportBookModel(k, data[k].totalCounter, links, largeFile, wrongImage);
      reports.push(currentReport);

      // reset.
      links = null;
      largeFile = null;
      wrongImage = null;
    }

    // Return reports.
    return reports;
  }

  deleteReportByBookId(bookID: string): Promise<any> {
   const deleteReportRef = this.reportRef.child(bookID);
   return deleteReportRef.remove();
  }

  decrementStadistic(reportedBook: ReportBookModel) {
    const stadisticRef = firebase.database().ref('stadistic/totalReports');
    stadisticRef.transaction(
      (totalReports) => {
        if (totalReports >= reportedBook.totalReportCounter) {
          totalReports -= reportedBook.totalReportCounter;
          return totalReports;
        }
        totalReports = 0;
        return totalReports;
      }
    );
  }
}
