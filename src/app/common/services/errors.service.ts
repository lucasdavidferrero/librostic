import * as firebase from 'firebase';

export class ErrorsService {
  private errorRef = firebase.database().ref('Errors');

  constructor() {
  }

  saveErrorWhenReportBook(err: Error) {
    this.setReason('reportBook').push(
      {
        name: err.name,
        message: err.message,
        stack: err.stack
      }
    );
  }


  fetchingBookError(err: Error) {
    this.setReason('fetchingBook').push(
      {
        name: err.name,
        message: err.message,
        stack: err.stack
      }
    );
  }

  saveError(ref: firebase.database.Reference, error: Error) {
    ref.push({
      name: error.name,
      message: error.message,
      stack: error.stack
    });
  }
  setReason(reason: string): firebase.database.Reference {
    switch (reason) {
      case 'reportBook':
        return this.errorRef.child('reportBook');
      case 'fetchingBook':
        return this.errorRef.child('fetchingBook');
      case 'search-result-book' :
        return this.errorRef.child('search-result-book');
      case 'fetchingReports' :
        return this.errorRef.child('fetchingReports');
      case 'onChangeBookLink' :
        return this.errorRef.child('onChangeBookLink');
      case 'onChangeBookImage' :
        return this.errorRef.child('onChangeBookLink');
      case 'fetchingCategories' :
        return this.errorRef.child('onChangeBookLink');
      default:
        return this.errorRef.child('other');
    }
  }

}
