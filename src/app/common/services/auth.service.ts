import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import {Router} from '@angular/router';
import { UserModel } from '../models/user.model';
import { UserService } from './user.service';


@Injectable()
export class AuthService {
  private _token: string = null;

  // We use first login in app.module to check if is the first login. If it is we skip the refresh token method.
  private _firstLogin = false;
  constructor(private afAuth: AngularFireAuth,
              private router: Router,
              private userService: UserService) {}


  get isFirstLogin() {
    return this._firstLogin;
  }
  get getUserToken(): string{
    return this._token;
  }
  set setUserToken(tk: string) {
    this._token = tk;
  }
  // We define the Facebook provider and passing it to signin(). We do this for each provider that we want to integrate.
  signinWithFacebook(): Promise<any> {
    const fbProvider = new firebase.auth.FacebookAuthProvider();
    return this.signin(this.afAuth.auth.signInWithPopup(fbProvider));
  }

  signinWithGoogle(): Promise<any> {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.signin(this.afAuth.auth.signInWithPopup(provider));
  }

  signinWithGithub(): Promise<any> {
    const provider = new firebase.auth.GithubAuthProvider();
    return this.signin(this.afAuth.auth.signInWithPopup(provider));
  }

  signinWithTwitter(): Promise<any> {
    const provider = new firebase.auth.TwitterAuthProvider();
    return this.signin(this.afAuth.auth.signInWithPopup(provider));
  }

  // If this method get resolved then we redirect the user to the home page and get the token.
  // Besides, when this method execute the reject() we catch it in the login component and we handle the errors there.
  // This method can be reusable across multiple providers such Facebook, Twitter, Github , etc.
  signin(popupResult: Promise<any>): Promise<any> {
    return popupResult
      .then(
        (res) => {
          this._firstLogin = true;
          const user: firebase.User = res.user.toJSON();
          const credential = res.credential;
          this._token = credential.accessToken;

          // Initialising the user and passing to the user service's property (_user)
          const providedData = user.providerData[0];
          // We use user.uid instead user.providerData[0].uid because the hash. By calling user.uid we get the uid hashed wich is more secure.
          const buildedUser = new UserModel(user.uid, providedData.displayName,
            providedData.email, providedData.photoURL, providedData.providerId);
          this.userService.user = buildedUser;


          // check if the user exists in the DB, if not we add it.
          const dbRef = firebase.database().ref().child(`users/${this.userService.user.uid}`);
          dbRef.once('value')
            .then(
              (snapshot: firebase.database.DataSnapshot) => {
                if (snapshot.exists()) {
                  // Check if the users if banned. If it is we logout.
                  firebase.database().ref(`users/${this.userService.user.uid}`).child('isBanned')
                    .once('value')
                    .then(
                      (snapshotIsBanned: firebase.database.DataSnapshot) => {
                        if (snapshotIsBanned.exists()) {
                          // At this point the value is equal true meaning that the user is indeed banned.
                          // Logout the user and redirect to /login with query params. Clear the user Service.
                          this.userService.user = null;
                          firebase.auth().signOut()
                            .then(
                              () => {
                                // Here redirect the user to login with bannedId query param.
                                this.router.navigate(['/login'], {queryParams: {bannedId: snapshotIsBanned.val()} } );
                              }
                            );
                        } else {
                          // The user is not banned. Everything is OK so navigate
                          this.router.navigate(['/']);
                        }
                      }
                    );
                } else {
                  // Add the user to the users node and redirect him.
                  firebase.database().ref('users')
                    .update( {
                      [this.userService.user.uid] : { displayName: this.userService.user.displayName,
                                                      email: this.userService.user.email,
                                                      photoURL:  this.userService.user.photoUrl,
                                                      providerId: this.userService.user.providerId,
                                                      joinDate: new Date(),
                                                      points: 0,
                                                    }
                    } )
                    .then(
                      () => {
                        this.router.navigate(['/']);
                        // Increment the value members on stadistic node.
                        const refStadistic = firebase.database().ref('stadistic/members');
                        refStadistic.transaction(
                          (members) => {
                            members += 1;
                            return members;
                          }
                        );
                      }
                    );
                }
              }
            );
        }
      );
  }

  // Refresh and get the token.
  refreshTokenAndGetIt() {
    firebase.auth().currentUser.getIdToken(true)
      .then(
        (tk: string) => {
          this._token = tk;
        }
      );
  }

  getCurrentUserToken() {
    firebase.auth().currentUser.getIdToken()
      .then(
        (tk: string) => {
           this._token = tk;
        }
      );
    return this._token;
  }

  isAdmin(uid: string) {
    const db = firebase.database().ref(`users/${uid}/isAdmin`);
    db.once('value')
      .then(
        (snapshot: firebase.database.DataSnapshot) => {
          if (snapshot.exists() && snapshot.val() ) {
            this.userService.setIsUserAdmin(true);
            this.userService.isAdmin = true;
          } else {
            this.userService.setIsUserAdmin(false);
            this.userService.isAdmin = false;
          }
        }
      );
  }

  isMod(uid: string) {
    // TODO terminar.
    const db = firebase.database().ref(`users/${uid}/isMod`);
  }

  signout() {
    this.afAuth.auth.signOut()
      .then(
        () => {
          this.userService.user = null;
          this.router.navigate(['/']);
        }
      );
  }
}
