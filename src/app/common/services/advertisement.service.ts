import * as firebase from 'firebase';

export class AdvertisementService {
  private advertisementRef = firebase.database().ref(`app/public/allowAD`);
  constructor() {}

  isAdEnabled(): Promise<any> {
    return this.advertisementRef.once('value');
  }

  changeAdToTrue(): Promise<any> {
    return this.advertisementRef.parent.update({
      allowAD: true
    });
  }

  changeAdToFalse(): Promise<any> {
    return this.advertisementRef.parent.update(
      {
        allowAD: false
      }
    );
  }
}
