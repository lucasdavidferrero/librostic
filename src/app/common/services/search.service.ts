import * as firebase from 'firebase';

export class SearchService {
  private bookRef = firebase.database().ref('books');
  private authorRef = firebase.database().ref('authors');
  private readonly MAX_RESULT = 45;

  constructor() {}

  // TODO En la próxima versión hacer paginación en vez de esto.
  searchByBookName(query: string): Promise<any> {
    return this.bookRef.orderByChild('queryableTitle').startAt(query).endAt(query + '\uf8ff').limitToFirst(this.MAX_RESULT)
      .once('value');
  }
}
