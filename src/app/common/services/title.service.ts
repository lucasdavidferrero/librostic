import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable()
export class TitleService {
  private readonly BRAND_NAME = 'Librostic';

  constructor(private titleService: Title) {}

  setTitle(newTitle: string) {
    this.titleService.setTitle(`${this.BRAND_NAME} - ${newTitle}`);
  }

  setDefaultTitle() {
    this.titleService.setTitle(this.BRAND_NAME);
  }
}
