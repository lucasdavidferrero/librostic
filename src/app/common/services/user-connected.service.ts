import {Injectable} from '@angular/core';
import * as firebase from 'firebase';
@Injectable()
export class UserConnectedService {

  updateDisplayName(userID: string, newDisplayName: string): Promise<any> {
    const dbRef = firebase.database().ref(`users/${userID}`);
      return dbRef.update(
        {
          displayName : newDisplayName
        }
      );
  }
  updateProfilePhoto(userID: string, file: any): firebase.storage.UploadTask {
    const storageRef = firebase.storage().ref(`user/${userID}/profile/profile_image`);
    return storageRef.put(file);
  }
  changeProfileURL(userID: string, newPhotoURL: string) {
    const userRef = firebase.database().ref(`users/${userID}`)
      .update(
        {
          photoURL: newPhotoURL
        }
      );
  }
}
