import { AuthorMetaModel } from './author-meta.model';
import { CategoryMetaModel } from './category-meta.model';

export class BookModel {
  constructor(public $key: string,
              public authors: AuthorMetaModel[],
              public briefDescription: string,
              public categories: CategoryMetaModel,
              public coverImage: string,
              public downloadOption: string,
              public downloadURL: string,
              public edition: number,
              public likes: number,
              public numberOfPages: number,
              public releaseDate: number,
              public title: string,
              public uploadedUser: string) {}
}
