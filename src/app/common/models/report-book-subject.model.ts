export class ReportBookSubjectModel {
  constructor(public subjectName: string,
              public reportCounter: number,
              public users: string[]) {}
}
