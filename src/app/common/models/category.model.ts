import { SubCategoryModel } from './sub-category.model';

export class CategoryModel {
  constructor(public key: string,
              public name: string,
              public heroImageURL: string,
              public subCategory?: SubCategoryModel[]) {
  }
}
