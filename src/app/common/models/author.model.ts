export class AuthorModel {
  constructor(public fullName: string,
              public briefBio: string,
              public $key: string,
              public profileURL?: string) {}
}
