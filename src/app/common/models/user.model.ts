import {UserMetaDataModel} from './user-meta-data.model';

export class UserModel {
  constructor(public uid: string,
              public displayName: string,
              public email: string,
              public photoUrl: string,
              public providerId: string,
              public userMetaData?: UserMetaDataModel) {}
}
