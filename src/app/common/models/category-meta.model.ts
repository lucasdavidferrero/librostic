export class CategoryMetaModel {
  constructor(public mainCategory: {key: string, name: string},
              public subCategory: {key: string, name: string}) {}
}
