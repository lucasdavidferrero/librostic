export class SubCategoryModel {
  constructor(public key: string,
              public name: string,
              public bookCounter?: number) {}
}
