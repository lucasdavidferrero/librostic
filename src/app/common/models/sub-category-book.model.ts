export class SubCategoryBookModel {
  constructor(public $key: string,
              public books: {key: string, coverImage: string, title?: string}[]) {}
}
