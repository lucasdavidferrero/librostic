import { ReportBookSubjectModel } from './report-book-subject.model';

export class ReportBookModel {
  constructor(public $keyBook,
              public totalReportCounter: number,
              public linkSubject?: ReportBookSubjectModel,
              public largeFileSubject?: ReportBookSubjectModel,
              public wrongImageSubject?: ReportBookSubjectModel) {}
}
