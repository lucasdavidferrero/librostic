export class AuthorMetaModel {
  constructor(public key: string,
              public name: string) {}
}
