export class LoginError {
  constructor(public title,
              public messageContent) {}
}
