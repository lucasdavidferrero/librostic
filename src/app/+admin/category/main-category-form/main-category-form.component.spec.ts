import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCategoryFormComponent } from './main-category-form.component';

describe('MainCategoryFormComponent', () => {
  let component: MainCategoryFormComponent;
  let fixture: ComponentFixture<MainCategoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainCategoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
