import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';
import { CategoryService } from '../../../common/services/category.service';
import { FileManager } from '../../../auth/file-manager';

@Component({
  selector: 'app-main-category-form',
  templateUrl: './main-category-form.component.html',
  styleUrls: ['./main-category-form.component.css']
})
export class MainCategoryFormComponent implements OnInit {
  successfullyAdded = false;
  failedToAdded = false;

  isSendingInformation = false;
  isValidImage = false;
  showErrorMessage = false;
  @ViewChild('imageControl') imageFile: ElementRef;
  @ViewChild('imgElement') previewImage: ElementRef;
  constructor(private categoryService: CategoryService,
              private fileManager: FileManager) { }

  ngOnInit() {
  }
  onChangeHeroImage() {
    this.isValidImage = false;
    const reader = new FileReader();
    const file: File = this.imageFile.nativeElement.files[0];
    const imgPreview = this.previewImage.nativeElement;
    reader.addEventListener('load', () => {
      imgPreview.src = reader.result;
    });
    reader.readAsDataURL(file);
    // Max 5MB
    if (this.fileManager.hasCorrectImageExtention(file.type) && !this.fileManager.hasPassedMaxBytes(file.size, 5242880)) {
      this.isValidImage = true;
    }
  }

  onSubmitMainCategory(f: FormGroup) {
    this.isSendingInformation = false;
    this.failedToAdded = false;
    this.successfullyAdded = false;
    const currentFile = this.imageFile.nativeElement.files[0];
    if ( f.valid && this.isValidImage) {
      this.isSendingInformation = true;
      const responseKey = this.categoryService.addMainCategory(f.controls.categoryName.value);
      if (responseKey) {
        this.categoryService.uploadMainCategoryHeroImage(currentFile, responseKey)
          .then(
            () => {
              this.categoryService.addNewSubcategory(responseKey, f.controls.subCategoryName.value)
                .then(
                  () => {
                    this.isSendingInformation = false;
                    this.successfullyAdded = true;
                    this.previewImage.nativeElement.src = '';
                    this.imageFile.nativeElement.value = '';
                    f.reset();
                  }
                );

            }
          )
          .catch(
            () => {
              this.isSendingInformation = false;
              this.failedToAdded = true;
            }
          );
      }
    } else {
      this.showErrorMessage = true;
    }
  }

  onCloseErrorMessage() {
    this.showErrorMessage = false;
  }
  onCloseErrorDatabase() {
    this.failedToAdded = false;
  }
}
