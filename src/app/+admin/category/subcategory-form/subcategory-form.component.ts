import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../../common/services/category.service';
import { CategoryModel } from '../../../common/models/category.model';

@Component({
  selector: 'app-subcategory-form',
  templateUrl: './subcategory-form.component.html',
  styleUrls: ['./subcategory-form.component.css']
})
export class SubcategoryFormComponent implements OnInit {
  subcategoryForm: FormGroup;
  mainCategories: CategoryModel[];
  successMessage = false;

  errorMessage = false;
  errorMessageBody = '';
  constructor(private fb: FormBuilder,
              private categoryService: CategoryService) {
  }
  ngOnInit() {
    this.categoryService.getAllMainCategories()
      .then(
        (snapshot) => {
          this.mainCategories = this.categoryService.fillCategoryModel(snapshot);
          this.mainCategories.sort((a, b) => {
            if (a.name < b.name) { return -1; }
            if (a.name > b.name) { return 1; }
            return 0;
          });
        }
      );
    this.buildForm();
  }

  private buildForm() {
    this.subcategoryForm = this.fb.group(
      {
        'main-category' : ['', Validators.compose([
          Validators.required
        ])],
        'sub-category-name' : ['', Validators.compose([
          Validators.required
        ])]
      }
    );
  }
  onSubcategorySend() {
    this.successMessage = false;
    this.errorMessage = false;
    const mainCategoryKey = this.subcategoryForm.controls['main-category'].value;
    const subCategoryName = this.subcategoryForm.controls['sub-category-name'].value;
    if (this.subcategoryForm.valid) {
      this.categoryService.addNewSubcategory(mainCategoryKey, subCategoryName).once('value')
        .then(
          () => {
            this.successMessage = true;
            this.subcategoryForm.reset();
          }
        )
        .catch(
          (err: Error) => {
            this.errorMessage = true;
            this.errorMessageBody = err.message;
          }
        );

    } else {
      this.errorMessage = true;
      this.errorMessageBody = 'Todos los campos son obligatorios';
    }
  }
  onCloseMessage() {
    this.successMessage = false;
    this.errorMessage = false;
  }
}
