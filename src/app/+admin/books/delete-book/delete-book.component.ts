import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BookService } from '../../../common/services/book.service';
import * as firebase from 'firebase';
import {BookModel} from '../../../common/models/book.model';
declare var $: any;

@Component({
  selector: 'app-delete-book',
  templateUrl: './delete-book.component.html',
  styleUrls: ['./delete-book.component.css']
})
export class DeleteBookComponent implements OnInit {
  @ViewChild('keyInput') keyBook: ElementRef;
  @ViewChild('masterKeyInput') masterKey: ElementRef;

  errorMessage: string;
  successMessage: string;
  book: BookModel;
  isLoading = false;
  constructor(private bookService: BookService) { }

  ngOnInit() {

  }
  onDeleteBook() {
    const bookID = this.keyBook.nativeElement.value.trim();
    if (bookID) {
      this.showModal();
    } else {

    }
  }

  onConfirmMasterPassword() {
    this.deleteBook();
    this.hideModalAndResetInput();
  }

  private deleteBook() {
    const masterKey = this.masterKey.nativeElement.value.trim();
    const bookID = this.keyBook.nativeElement.value.trim();

    // Validate with the master key.
    this.isLoading = true;
    this.bookService.allowAction(masterKey)
      .then(
        (snap: firebase.database.DataSnapshot) => {
          if (snap.val() === masterKey) {
            // Erase the book...
            this.eraseBook(bookID);
          } else {
            // Show error that the password doesn't match.
            this.errorMessage = 'La contraseña es incorrecta.'
            this.isLoading = false;
          }
        }
      );
  }

  private eraseBook(bookID: string) {
    this.bookService.getBookByKey(bookID)
      .then(
        (snap: firebase.database.DataSnapshot) => {
          if (snap.val()) {
            this.book = this.bookService.fillBookObject(snap);

            // Now we delete everything that is attached with the book.
            this.bookService.deleteImageFromStorage(this.book.$key)
              .then(
                () => {
                  this.bookService.deleteBookFromSubCategory(this.book.categories.subCategory.key, this.book.$key)
                    .then(
                      () => {
                        for (let i = 0; i < this.book.authors.length; i++) {
                          const authorKey = this.book.authors[i].key;
                          this.bookService.deleteBookFromAuthor(authorKey, this.book.$key)
                            .then(
                              () => {
                                console.log('Author deleted: ',authorKey);
                              }
                            )
                            .catch(
                              (error: Error) => {
                                console.error(error);
                              }
                            );
                        }
                        this.bookService.deleteBook(this.book.$key)
                          .then(
                            () => {
                              this.bookService.decrementSubCategoryCounter(this.book.categories.mainCategory.key
                                , this.book.categories.subCategory.key);
                              this.bookService.decrementUploadedBooks();

                              if (this.book.downloadOption === 'firebaseStorage') {
                                this.bookService.deletePDFfromStorage(this.book.$key)
                                  .then(
                                    () => {
                                      this.successfullyDelete();
                                    }
                                  );
                              } else {
                                this.successfullyDelete();
                              }
                            }
                          );
                      }
                    )
                    .catch();
                }
              )
              .catch(
                (err: Error) => {
                  console.error(err);
                }
              );
          } else {
            this.errorMessage = `No existe ningún libro con el KEY: ${bookID}`;
          }
        }
      );
  }

  private successfullyDelete() {
    this.isLoading = false;
    this.successMessage = `El Libro ${this.book.title} fué eliminado exitosamente.`;
    this.book = null;
    this.keyBook.nativeElement.value = '';
  }

  private showModal() {
    $('.tiny.modal')
      .modal('show')
    ;
  }

  hideModalAndResetInput() {
    this.masterKey.nativeElement.value = '';
    $('.tiny.modal')
      .modal('hide')
    ;
  }

  hideMessages() {
    this.successMessage = '';
    this.errorMessage = '';
  }
}
