import { Component, OnInit, ViewChild } from '@angular/core';
import { BookService } from '../../../common/services/book.service';
import * as firebase from 'firebase';
import { NgForm } from '@angular/forms';
import { ErrorsService } from '../../../common/services/errors.service';
import { FormBookService } from '../../../common/services/form-book.service';
import { FileManager } from '../../../auth/file-manager';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {
  @ViewChild('bookID') bookIdInput: any;
  bookKey: string;
  bookImageUrl: string;

  showEditLinkForm = false;
  showEditImageForm: boolean;
  errorMesagge: string;
  successMessage: string;
  isLoading = false;


  constructor(private bookService: BookService,
              private errorService: ErrorsService,
              private formBookService: FormBookService,
              private fileManager: FileManager) { }

  ngOnInit() {

  }

  onEditLink(input: string) {
    this.resetMessages();
    if (input) {
      this.isLoading = true;
      this.bookKey = input.trim();
      this.bookService.getBookByKey(this.bookKey)
        .then(
          (snap: firebase.database.DataSnapshot) => {
            if (snap.val()) {
              this.showEditImageForm = false;
              this.showEditLinkForm = true;
              this.isLoading = false;
            } else {
              this.errorMesagge = `No encontramos ningún libro con el KEY: ${this.bookKey}`;
              this.bookKey = '';
              this.isLoading = false;
            }
          }
        );
    }
  }
  onEditImage(input: string) {
    this.resetMessages();
    if (input) {
      this.bookKey = input.trim();
      this.isLoading = true;
      this.bookService.getBookByKey(this.bookKey)
        .then(
          (snap: firebase.database.DataSnapshot) => {
            if (snap.val()) {
              this.showEditLinkForm = false;
              this.showEditImageForm = true;
              // store the image to show it in the UI.
              this.bookImageUrl = snap.val().coverImage;
              this.isLoading = false;
            } else {
              this.errorMesagge = `No encontramos ningún libro con el KEY: ${this.bookKey}`;
              this.bookKey = '';
              this.isLoading = false;
            }
          }
        );
    }
  }

  onSubmitEditLink(form: NgForm) {
    this.resetMessages();
    if (form.valid && this.bookKey) {
      const newKey = form.controls.googleDriveInput.value;
      this.isLoading = true;
      this.bookService.updateLinkToGoogleDrive(newKey, this.bookKey)
        .then(
          () => {
            this.isLoading = false;
            this.successMessage = 'Link de PDF actualizado correctamente.';
            form.reset();
            this.showEditLinkForm = false;
            this.bookIdInput.nativeElement.value = '';
            this.bookKey = '';
          }
        )
        .catch(
          (err: Error) => {
            this.errorService.saveError(this.errorService.setReason('onChangeBookLink'), err);
            this.errorMesagge = err.message;
            this.isLoading = false;
          }
        );
    }
  }

  onUpdateCoverImage(newImage: File) {
    if (this.fileManager.hasCorrectImageExtention(newImage.type) &&
        !this.fileManager.hasPassedMaxBytes(newImage.size, 629145)) {
        this.isLoading = true;
        this.formBookService.saveCoverImage(newImage, this.bookKey)
          .then(
            (snap) => {
              this.bookService.updateCoverImageURL(snap.downloadURL, this.bookKey)
                .then(
                  () => {
                    // everything OK. reset fields and show message
                    this.successMessage = 'Imágen actualizada correctamente.';
                    this.isLoading = false;
                    this.showEditImageForm = false;
                    this.bookKey = '';
                  }
                ).catch(
                (error: Error) => {
                  this.saveImageError(error);
                }
              );
            }
          ).catch(
          (err: Error) => {
            this.saveImageError(err);
          }
        );
    } else {
      this.isLoading = false;
      this.errorMesagge = 'El archivo que intentas cambiar no es una imágen o es muy pesado (307KB máximo permitido)';
    }
  }
  private saveImageError(error: Error) {
    this.errorService.saveError(this.errorService.setReason('onChangeBookImage'), error);
    this.errorMesagge = error.message;
    this.isLoading = false;
  }
  resetMessages() {
    this.successMessage = '';
    this.errorMesagge = '';
  }
}
