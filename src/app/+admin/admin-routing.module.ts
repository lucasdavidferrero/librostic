import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainCategoryFormComponent } from './category/main-category-form/main-category-form.component';
import { WelcomeMessageComponent } from './welcome-message/welcome-message.component';
import { SubcategoryFormComponent } from './category/subcategory-form/subcategory-form.component';
import { ListUsersComponent } from './users/list-users/list-users.component';
import { BanUsersComponent } from './users/ban-users/ban-users.component';
import { TopReportsComponent } from './reports/top-reports/top-reports.component';
import { BookEditComponent } from './books/book-edit/book-edit.component';
import { DeleteBookComponent } from './books/delete-book/delete-book.component';
import {AuthGuardService} from '../auth/auth-guard.service';
import {AdminGuardService} from './admin-guard.service';
import {AdvertisementComponent} from '../shared/advertisement/advertisement.component';
import {AdComponent} from './advertisement/ad/ad.component';

const adminRoutes: Routes = [
  { path: '' , component: DashboardComponent, canActivate:[AuthGuardService] , children : [
    { path: 'welcome', component: WelcomeMessageComponent },

    // user routes
    { path: 'list-users', component: ListUsersComponent },
    { path: 'ban-user', component: BanUsersComponent },

    // category routes
    { path: 'new-main-category', component: MainCategoryFormComponent },
    { path: 'new-subcategory', component: SubcategoryFormComponent },

      // Report routes
      { path: 'top-reports', component: TopReportsComponent },

      // Book routes
      { path: 'book-edit', component: BookEditComponent },
      { path: 'book-delete', component: DeleteBookComponent },

      // AD
      { path: 'advertisement', component: AdComponent }
  ]}
];

@NgModule({
  imports : [RouterModule.forChild(adminRoutes)],
  exports : [RouterModule]
})
export class AdminRoutingModule {}
