import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { SubMenuComponent } from './sub-menu/sub-menu.component';
import { MainCategoryFormComponent } from './category/main-category-form/main-category-form.component';
import { WelcomeMessageComponent } from './welcome-message/welcome-message.component';
import { SubcategoryFormComponent } from './category/subcategory-form/subcategory-form.component';
import { ListUsersComponent } from './users/list-users/list-users.component';
import { BanUsersComponent } from './users/ban-users/ban-users.component';
import { TopReportsComponent } from './reports/top-reports/top-reports.component';
import { BookEditComponent } from './books/book-edit/book-edit.component';
import { DeleteBookComponent } from './books/delete-book/delete-book.component';
import { AdComponent } from './advertisement/ad/ad.component';


@NgModule({
  declarations : [
    DashboardComponent,
    SubMenuComponent,
    MainCategoryFormComponent,
    WelcomeMessageComponent,
    SubcategoryFormComponent,
    ListUsersComponent,
    BanUsersComponent,
    TopReportsComponent,
    BookEditComponent,
    DeleteBookComponent,
    AdComponent,
    ],
  imports : [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule
  ]
})
export class AdminModule {}
