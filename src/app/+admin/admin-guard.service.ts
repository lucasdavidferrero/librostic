import { CanLoad, Route, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import {UserService} from '../common/services/user.service';

@Injectable()
export class AdminGuardService implements CanLoad {
  constructor(private userService: UserService,
              private router: Router) {}
  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    console.log(this.userService.isAdmin);

    if (this.userService.isAdmin === undefined) {
      this.router.navigate(['']);
      return false;
    }

    return this.userService.isAdmin;
  }
}

