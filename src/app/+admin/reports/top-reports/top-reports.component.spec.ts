import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopReportsComponent } from './top-reports.component';

describe('TopReportsComponent', () => {
  let component: TopReportsComponent;
  let fixture: ComponentFixture<TopReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
