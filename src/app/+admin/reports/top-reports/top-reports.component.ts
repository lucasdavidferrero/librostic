import { Component, OnInit } from '@angular/core';
import { BookReportService } from '../../../common/services/book-report.service';
import * as firebase from 'firebase';
import {ReportBookModel} from '../../../common/models/report-book.model';
import {ErrorsService} from '../../../common/services/errors.service';

declare var $: any;

@Component({
  selector: 'app-top-reports',
  templateUrl: './top-reports.component.html',
  styleUrls: ['./top-reports.component.css']
})
export class TopReportsComponent implements OnInit {
  reports: ReportBookModel[];
  isLoading = false;

  selectedBook: ReportBookModel;
  constructor(private bookReportService: BookReportService,
              private errorService: ErrorsService) { }

  ngOnInit() {
    this.isLoading = true;
    this.bookReportService.get20MostReported()
      .then(
        (snap: firebase.database.DataSnapshot) => {
          if (snap.val()) {
            this.reports = this.bookReportService.transformResponseToArray(snap.val());
          }
          this.isLoading = false;
        }
      );
  }
  showModal() {
    $('.ui.basic.modal')
      .modal('show')
    ;
  }
  onCheckClick(selectedBook: ReportBookModel) {
    this.showModal();
    this.selectedBook = selectedBook;
  }

  onClickDeleteReport() {
    if (this.selectedBook) {
      this.isLoading = true;
      this.bookReportService.deleteReportByBookId(this.selectedBook.$keyBook)
        .then(
          () => {
            if (this.reports.length === 1) {
              this.reports = [];
            } else {
              this.reports = this.reports.slice(this.reports.indexOf(this.selectedBook), 1);
            }
            this.bookReportService.decrementStadistic(this.selectedBook);
            this.selectedBook = null;
            this.isLoading = false;
          }
        )
        .catch(
          (err: Error) => {
            this.isLoading = false;
            this.errorService.saveError(this.errorService.setReason('fetchingReports'), err);
          }
        );
    }
  }
}
