import { Component, OnInit } from '@angular/core';
import { DashboardUsersService } from '../../../common/services/dashboard/dashboard-users.service';
import {UserModel} from '../../../common/models/user.model';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  isLoading: boolean;
  userList: UserModel[];
  constructor(private dashBoardUsersService: DashboardUsersService) { }

  ngOnInit() {
    this.isLoading = true;
    this.dashBoardUsersService.filterListUser('mostRecent')
      .then(
        () => {
          this.isLoading = false;
          this.userList = this.dashBoardUsersService.usersFound;
        }
      )
      .catch();
  }
  onNextList() {
    this.isLoading = true;
    // Cambiar mostRecent por el valor seleccionado.
    this.dashBoardUsersService.filterListUser('mostRecent', this.userList[this.userList.length - 1].uid)
      .then(
        () => {
          this.isLoading = false;
          this.userList = this.dashBoardUsersService.usersFound;
        }
      )
      .catch();
  }
  onBackList() {
    // TODO next  version.
  }
}
