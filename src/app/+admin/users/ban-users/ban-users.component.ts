import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UserService } from '../../../common/services/user.service';
import { BanUserService } from '../../../common/services/dashboard/ban-user.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-ban-users',
  templateUrl: './ban-users.component.html',
  styleUrls: ['./ban-users.component.css']
})
export class BanUsersComponent implements OnInit {
  errorMessage: {title: string, body: string};
  showError = false;
  showSuccess = false;
  constructor(private userService: UserService,
              private banUserService: BanUserService) { }

  ngOnInit() {
  }

  onSubmitBan(form: FormGroup) {
    this.showError = false;
    this.showSuccess = false;
    const formValues = form.value;
    if (form.valid) {
      if (this.userService.user.uid !== formValues.uid) {
        this.banUserService.userExist(formValues.uid)
          .then(
            (snapshot: firebase.database.DataSnapshot) => {
              if (snapshot.val()) {
                // Check if the user is admin or mod.
                const userEmail = snapshot.val().email;
                this.banUserService.checkIfTheUserIsStaff(formValues.uid)
                  .then(
                    () => {
                      if (!this.banUserService.isStaff) {
                        // Block the user.
                        this.banUserService.blockUser(formValues.uid, userEmail, formValues.description)
                          .then(
                            (response: firebase.database.ThenableReference) => {
                              // Success Message
                              this.banUserService.changeIsBannedToTrue(formValues.uid, response.key)
                                .then(
                                  () => {
                                    this.showSuccess = true;
                                  }
                                );
                            }
                          );
                      } else {
                        // Show message. The UID is node Mod or Admin.
                        this.errorMessage = { title: 'No puedes bloquear usuario del Staff.',
                          body: 'Tu no puedes bloquear usuarios del Staff. Solo el superadmin puede.' };
                        this.showError = true;
                      }
                    }
                  );
              } else {
                this.errorMessage = { title: 'El usuario no existe.',
                                      body: 'El ID del usuario no esta registrado en el sistema.' };
                this.showError = true;
              }
            }
          )
          .catch();
      } else {
        this.errorMessage = { title: 'No puedes bloquearte a tí mismo.',
                              body: 'El UID es el de tu cuenta!' };
        this.showError = true;
      }
    }
  }

  onCloseMessage() {
    this.showSuccess = false;
    this.showError = false;
  }
}
