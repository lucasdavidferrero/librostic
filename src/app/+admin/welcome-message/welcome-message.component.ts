import { Component, OnInit } from '@angular/core';
import { UserService } from '../../common/services/user.service';
import { WelcomeService } from '../../common/services/dashboard/welcome.service';
import { StadisticModel } from '../../common/models/stadistic.model';
import * as firebase from 'firebase';

@Component({
  selector: 'app-welcome-message',
  templateUrl: './welcome-message.component.html',
  styleUrls: ['./welcome-message.component.css']
})
export class WelcomeMessageComponent implements OnInit {
  stadistic: StadisticModel;
  constructor(public userService: UserService,
              private welcomeService: WelcomeService) { }

  ngOnInit() {
    this.welcomeService.getStadistics()
      .then(
        (snapshot: firebase.database.DataSnapshot) => {
          const data = snapshot.val();
          if (data) {
            this.stadistic = new StadisticModel(data.uploadedBooks, data.members);
          }
        }
      );
  }

}
