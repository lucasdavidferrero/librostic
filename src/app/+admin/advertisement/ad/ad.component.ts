import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AdvertisementService } from '../../../common/services/advertisement.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.css']
})
export class AdComponent implements OnInit {
  @ViewChild('inputCheck') checkbox: ElementRef;
  isLoading = false;
  constructor(private adService: AdvertisementService) { }

  ngOnInit() {
    this.isLoading = true;
    this.adService.isAdEnabled()
      .then(
        (snap: firebase.database.DataSnapshot) => {
          this.checkbox.nativeElement.checked = snap.val();
          this.isLoading = false;
        }
      ).catch(
      (err: Error) => {
        console.error(err);
        this.isLoading = false;
      }
    );
  }
  onSaveChanges() {
    const checkboxValue = this.checkbox.nativeElement.checked;
    if (checkboxValue) {
      this.isLoading = true;
      this.adService.changeAdToTrue()
        .then(
          () => {
            this.isLoading = false;
          }
        );
    } else {
      this.isLoading = true;
      this.adService.changeAdToFalse()
        .then(
          () => {
            this.isLoading = false;
          }
        )
        .catch(
          (err: Error) => {
            this.isLoading = false;
            console.error(err);
          }
        );
    }
  }
}
