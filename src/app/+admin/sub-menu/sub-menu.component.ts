import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.css']
})
export class SubMenuComponent implements OnInit {
  totalReports: number;

  constructor() { }

  ngOnInit() {
    this.listenOnReports();
  }


  private listenOnReports() {
    const stadisticRef = firebase.database().ref('stadistic/totalReports');
    stadisticRef.on('value', (snap: firebase.database.DataSnapshot) => {
      this.totalReports = snap.val();
    });
  }
}
